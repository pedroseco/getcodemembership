# GetcodeMembership plugin for CakePHP

## Requisitos
GetcodePayments

## EventListeners

- MembershipListener e SubscriptionsListener

## Utilização

*Plans*
Representa o produto/serviço.
Inclui preço, descrição e outra informação relevante.

*Subscriptions*
São as subscrições ativas e pertecem a um membro e contêm um plano.
A subscrição tem um prazo de duração e um campo de data de renovação.

- Quando uma subscrição é gerada, um invoice é criado com data de pagamento e valor. (generateInvoiceAfterCreate())



*Members*
Sócios e membros. Podem pertencer ou não a um User da plataforma.

## CRON
O plugin utiliza métodos para validar as datas das subscrições.
Necessário um CRON JOB diário às 00:00 para correr o Shell script.
```
bash bin/cake Subscriptions verifyEndOfTermSubscriptions
```

## Shell Scripts
```
verifyEndOfTermSubscriptions()
```
Valida todas as subscrições que estao a finalizar no dia de hoje.



## Tabelas
Adicionamos um plugin que permite obter informações da paginação nos resultados JSON
```
    $this->loadComponent('BryanCrowe/ApiPagination.ApiPagination');
```

Podemos usar o VueJS no frontend para criar tabelas com o plugin vue-tables2 https://github.com/ratiw/vuetable-2/
Para tal necessitamos de alterar os index() no cakephp para conter propriedades de filtrage:
```
    if($this->request->query['filter']){
        $this->paginate['conditions'] = [
            'title LIKE' => '%'.$this->request->query['filter'].'%'
        ];
    }
```
No initialize() do controller precisamos tambem de recorrer a um pequeno hack que permite obter o sort e direction
quando o vue-tables usa o sortable.
```
    if(isset($this->request->query['sort'])){
        $res=explode('#',$this->request->query['sort']);
        if(!empty($res[1])){
            $this->request->query['sort'] = $res[0];
            $this->request->query['direction']= $res[1];
        }
    }
```
Assim garantimos que o CAKE recebe os parâmetros corretos visto que no frontend colocamos um override no vue-tables para
que o query param do sort venha dividido com #.

## Settings
 > Settings.Subscriptions.renewal_days_before
 - Valido para calcular quantos dias antes uam subscrição deve ser renovada. É usado na Shell script