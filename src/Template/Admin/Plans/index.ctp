<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
      <h2>Lista de Produtos</h2>
    </div>
  </div>
<?php $this->end(); ?>

<div class="row justify-content-md-center" id="vuejs_instance">
  <div class="col col-md-10">

    <div class="row">

      <div class="col col-lg-3">

          <?=$this->Html->link('ADICIONAR',
            ['controller'=>'plans','action'=>'add'],
            ['class'=>'btn btn-md btn-primary'])
          ?>

      </div>

      <div class="col col-md-3">
      </div>


      <div class="col col-md-6">
        <div class="pull-right">
            <input v-on:keyup="setFilter(filterText)" v-model="filterText" class="form-control" type="search" placeholder="Pesquisa na tabela...">
        </div>
      </div>

    </div>

    <div class="row mt-3">
      <div class="col">

        <div class="card">
        <div class="card-body">
            <!-- TABLE -->
            <vuetable 
                ref="vuetable" 
                api-url="/admin/planos/index.json" 
                :fields="fields" 
                :per-page="15"
                :css="css.table"
                :sort-order="sortOrder"
                :query-params="{ sort: 'sort', order: 'direction', page: 'page', perPage: 'limit' }"
                :append-params="moreParams"
                pagination-path="pagination" 
                @vuetable:checkbox-toggled="toggledChbox($event)"
                @vuetable:pagination-data="onPaginationData">

                <template slot="actions" scope="props">
                    <div class="text-right">
                        <div class="text-right">
                            <button class="btn btn-outline-primary btn-sm" @click="editRow(props.rowData)">
                                Editar
                            </button>
                        </div>    
                    </div>
                </template>

                <template slot="member_link" scope="props">
                    <a href="#" @click="viewRow(props.rowData)">{{props.rowData.name}}</a>
                </template>

            </vuetable>

        </div>
        </div>

        <div class="mt-2 vue_table_footer">

            <button class="btn btn-sm btn-outline-secondary mt-3 mb-4" v-on:click="deleteSelected">Eliminar selecionados</div>

            <vuetable-pagination 
                ref="pagination" 
                :css="css.pagination"
                @vuetable-pagination:change-page="onChangePage">
            </vuetable-pagination>

        </div>

      </div>
    </div>


  </div>
</div>

<script src="https://unpkg.com/moment@2.20.1"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.6/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.1/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/babel-preset-es2015@6.24.1/lib/index.min.js"></script>
<script src="https://unpkg.com/vuetable-2@1.6.0"></script>

<script>
Vue.use(Vuetable)

new Vue({
    el: '#vuejs_instance',
    components:{
        'vuetable-pagination': Vuetable.VuetablePagination
    },
    data: {
        filterText: '',
        moreParams: {},
        sortOrder: [
            { field: 'name', direction: 'asc' }
        ],
        fields: [
            '__checkbox',
            {
                name: 'name',
                title: 'Nome',
                sortField: 'name'
            },
            {
                name: 'interval_num',
                title: 'Duração',
                sortField: 'interval_num'
            },
            '__slot:actions'
        ],
        css: {
            table: {
                tableClass: 'table  table-hover',
                loadingClass: 'loading',
                ascendingIcon: 'fa fa-chevron-up',
                descendingIcon: 'fa fa-chevron-down',
                handleIcon: 'fa fa-menu-hamburger',
            },
            pagination: {
                infoClass: 'pull-left',
                wrapperClass: 'vuetable-pagination pull-right',
                activeClass: 'btn btn-outline-default',
                disabledClass: 'disabled',
                pageClass: 'btn btn-border',
                linkClass: 'btn btn-border',
                icons: {
                    first: '',
                    prev: '',
                    next: '',
                    last: '',
                },
            }
        }
    },
    methods: {
        transform: function(d) {
            //alteramos o formato dos dados porque o Cake devolve com nomes
            //diferentes do necessario para o vue-table
            var transformed = {}

            transformed.pagination = {
                total: d.pagination.count,
                per_page: d.pagination.perPage,
                current_page: d.pagination.page,
                last_page: d.pagination.pageCount,
                next_page_url: d.pagination.nextPage,
                prev_page_url: d.pagination.prevPage
            }
            
            //temos de adicionar também os dados normais ao novo objecto
            transformed.data = d.data;

            return transformed
        },
        transformDate (value) {
            return moment(value).format("DD-MM-YYYY")
        },
        transformInterval (value) {
            return value==1?'<span class="badge badge-primary">Ativo</span>':'<span class="badge badge-warning">Inativo</span>'
        },
        //https://ratiw.github.io/vuetable-2/#/Sorting?id=overriding-the-sort-query-string
        getSortParam: function(sortOrder) {
            //requer um *hack* no controller do cakephp
            return sortOrder.map(function(sort) {
                return sort.field+'#'+sort.direction
            }).join(',')
            
        },
        toggledChbox(payload, dataItem) {
            console.log('row: '+this.$refs.vuetable.selectedTo)
        },
        onPaginationData (paginationData) {
            this.$refs.pagination.setPaginationData(paginationData)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        editRow(rowData){
            window.location.href = '/admin/planos/edit/'+rowData.id
        },
        setFilter (filterText) {
            console.log(filterText)
            this.moreParams = {
                'filter': filterText,                
            }
            Vue.nextTick( () => this.$refs.vuetable.refresh())
        },
        deleteSelected(){
            var selectedRows = this.$refs.vuetable.selectedTo
            var vtable = this.$refs.vuetable;
            if(selectedRows.length > 0){
                if(confirm('Pretende eliminar o(s) item(s) selecionado(s)?')){
                    axios.post('/admin/planos/delete', {
                        ids: selectedRows
                    })
                    .then(function (response) {
                        vtable.refresh()
                        console.log('success: '+response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                    vtable.refresh()
                }
            }
        }
    }
})
</script>