<?php $this->append('top_bar'); ?>
    <h2>Confirmar importação</h2>
<?php $this->end(); ?>


<?=$this->Form->create('ConfirmImport', ['url'=>['controller'=>'members','action'=>'import_success']])?>
<?php echo $this->Form->input('uploadfile', ['type'=>'hidden','value'=>$uploadfile]) ?>
<div class="row justify-content-md-center">
    <div class="col col-md-10">

        <div class="card">
            <div class="card-body">
                <div class="alert alert-info">
                    <span>Confirme a importação de dados, conferindo as colunas da amostra de dados em baixo apresentada. Volte a enviar o ficheiro caso não corresponda ao pretendido.</span>
                </div>

                <p>Serão importados <strong><?=count($members)?></strong> registo(s) válidos. <p>
                <table>
                    <thead>
                        <tr>
                        <?php foreach($members[0] as $head=>$value): ?>
                            <th><?php echo $head ?></th>
                        <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    $i=0;
                    foreach($members as $k=>$v): 
                    if($i <= 15):
                    ?>
                        <tr>
                        <?php foreach($v as $head=>$value): ?>
                            <td><?php echo $value ?></td>
                        <?php endforeach; ?>
                        </tr>
                    <?php 
                    endif;
                    $i++;
                    endforeach; ?>
                    </tbody>
                </table>
            
            </div>
        </div>

        <div class="box bg-white">
            <?=$this->Form->button('<i class="fa fa-fw fa-fw fa-save"></i> '.__('Confirmar Importação'),['type'=>'submit','class'=>'btn btn-success','escape'=>false]);?>
            <?=$this->Html->link(__('Cancelar'),['controller'=>'members','action'=>'import'],['class'=>'btn btn-warning','escape'=>false]);?>
            <div class="clearfix"></div>
        </div>

    </div>
</div>
<?=$this->Form->end()?>