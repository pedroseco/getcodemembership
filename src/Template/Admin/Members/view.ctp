<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
        <h2>Ficha de Membro</h2>
    </div>
  </div>
<?php $this->end(); ?>


<div class="row justify-content-md-center">

  <div class="col col-md-10">

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"><a class="nav-link active" href="#tab_geral" role="tab" data-toggle="tab">Geral</a></li>
        <li class="nav-item"><a class="nav-link" href="#tab_transactions" role="tab" data-toggle="tab">Transações</a></li>
    </ul>

    <div class="tab-content">
        <div class="card tab-pane fade show active" id="tab_geral" role="tabpanel" aria-labelledby="tab_geral">
            <div class="card-body">
                <div class="row">
                
                    <div class="col-md-4 col">

                        <div class="media mb-4">
                            <i class="mr-3 fa fa-4x fa-user"></i>
                            <div class="media-body">
                                <h5 class="mt-0"><?= h($member->name) ?></h5>
                                Desde: <?= $this->Time->format($member->admission_date,'dd-MM-yyyy') ?> | <?=$this->Html->link('Editar', ['controller'=>'Members','action'=>'edit',$member->id])?>
                            </div>
                        </div>

                        <ul id="accordion" class="card-accordion list-unstyled">

                            <div class="mb-0 card">
                                <div class="card-header">
                                    <li data-toggle="collapse" class="accordion-item is-active" data-target="#collapseOne">
                                        <a href="#" class="accordion-title">Morada</a>
                                    </li>
                                </div>
                                <div id="collapseOne" class="collapse show" data-parent="#accordion">
                                    <p>Rua: <?=h($member->address)?><br/> 
                                    Localidade: <?=h($member->local)?></p>
                                </div>
                            </div>

                            <div class="mb-0 card">
                                <div class="card-header">
                                    <li data-toggle="collapse" class="accordion-item" data-target="#collapseTwo">
                                        <a href="#" class="accordion-title">Outros detalhes</a>
                                    </li>
                                </div>
                                <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                    <p>Telefone: <?= h($member->phone) ?></p>
                                    <p>Email: <?= h($member->email) ?></p>
                                    <p>Membro desde: <?= h($member->admission_date) ?></p>
                                    <p>Registo criado: <?=h($member->created)?></p>
                                    <p>Registo modificado: <?=h($member->modified)?></p>
                                </div>
                            </div>
                            
                        </ul>

                    </div>

                    <div class="col-md-8 col" >

                        <ul class="list-inline custom-card-info">
                            <li class="list-inline-item text-center">
                                <h6 class="text-muted">Total pago</h6>
                                <div class="stat"><?=\Cake\I18n\Number::currency($due_total,'EUR')?></div>
                            </li>
                            <li class="list-inline-item text-center">
                                <h6 class="text-orange">Total em dívida</h6>
                                <div class="stat"><?=\Cake\I18n\Number::currency($paid_total,'EUR')?></div>
                            </li>
                            <li class="list-inline-item text-center">
                                <h6 class="text-muted">Próxima data pagamento</h6>
                                <div class="stat">Due On Receipt</div>
                            </li>
                        </ul>


                        <div class="box-log-activities p-2 mt-2">
                            <p>
                            Data último pagamento:  <?= $this->Time->format($member->last_payment_date,'dd-MM-yyyy') ?>
                            |
                            Valor: <?=\Cake\I18n\Number::currency($member->last_payment_value,'EUR')?></p>
                        </div>


                        <?php 
                        if(!empty($member->subscriptions)):
                            foreach($member->subscriptions as $subscription):
                        ?>


                        <div class="card mb-3 w-75">
                            <div class="card-body">
                                <div class="card-title">
                                    <?= $this->Html->link($subscription->name,['controller'=>'Subscriptions','action'=>'view',$subscription->id])?> | Periodicidade <?=$subscription->interval_num?> <?=$subscription->interval_unit?>
                                    <span class="badge badge-secondary float-right"><?=\Cake\Core\Configure::read('subscriptions_status')[$subscription->status] ?></span>
                                </div>
                                <div class="card-text">
                                    <p>
                                    <span class="text-muted">Valor:</span> <?=\Cake\I18n\Number::currency($subscription->sub_total,'EUR')?> 
                                    
                                    <span class="text-muted"><strong>Início: </strong> </span> <span><?= $this->Time->format($subscription->current_term_start_at,'dd-MM-yyyy') ?></span>  
                                    <span class="text-muted"><strong>Fim: </strong> </span> <span><?= $this->Time->format($subscription->current_term_end_at,'dd-MM-yyyy') ?></span> 
                                    </p>
                                </div>
                            </div>
                        </div>
                        <?php
                            endforeach; 
                        endif; 
                        ?>
                    
                    </div>

                </div>
            </div>
        </div>

        <div class="card tab-pane fade" id="tab_transactions">
            <div class="card-body">
                <div class="alert alert-info">
                    <span>Tabela de documentos associados ao membro. Clique num nº de doc para ver mais detalhes. </span>
                </div>
                <table class="table table-condensed">
                    <thead>
                        <th>Invoice #</th>
                        <th>Data</th>
                        <th>Valor</th>
                        <th>Estado</th>
                        <th></th>
                    </thead>
                    <tbody>
                    <?php if(!empty($invoices)): ?>
                        <?php foreach($invoices as $i): ?>
                        <tr>
                            <td><?= $this->Html->link($i['invoice_number'], ['plugin'=>'GetcodePayments','controller'=>'Invoices','action'=>'view',$i['id']])?></td>
                            <td><?=$i['due_date']?></td>
                            <td><?=\Cake\I18n\Number::currency($i['total'],'EUR')?></td>
                            <td><?=\Cake\Core\Configure::read('invoice_status')[$i['status']] ?></td>
                            <td>s<?= $this->Html->link('Consultar', ['controller'=>'invoices','action'=>'view',$i->id, 'plugin'=>'GetcodePayments'],['class'=>'btn btn-sm btn-primary'])?></td>
                        </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>    
                    </tbody>
                </table>
            </div>
        </div>
    </div>

  </div>

</div>


<script type="text/javascript">
    $('#loadingIcon').hide('fast');
</script>