<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
        <h2>Editar Membro</h2>
    </div>
  </div>
<?php $this->end(); ?>


<div class="d-none d-print-block">
    <h4>Informações de membro: <strong><?= $member->name ?></strong></h4>
    <hr>
</div>

<?php echo $this->element('form_member'); ?>
