<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
        <h2>Lista de Membros</h2>
    </div>
  </div>
<?php $this->end(); ?>

<div class="row justify-content-md-center" id="vuejs_instance">
  <div class="col col-md-10">

    <div v-if="flashmsg">
        <div class="alert alert-info"><span v-html="flashmsg"></span></div>
    </div>

    <div class="row">

      <div class="col col-md-6">
        <?php echo $this->Html->link('ADICIONAR', ['controller'=>'members','action'=>'add'], ['class'=>'btn btn-md btn-primary mr-2']); ?>
        <a href="/admin/membros/export.csv" class="btn btn-md btn-outline-secondary mr-2">EXPORTAR CSV</a>
        <?php echo $this->Html->link('IMPORTAR CSV', $this->Url->build(['controller' => 'members', 'action' => 'import']), ['class'=>'btn btn-md btn-outline-secondary']); ?>
      </div>

      <div class="col col-md-6">
        <div class="input-group">
            <input v-model="filterTextData" type="search" class="form-control" placeholder="Pesquisa na tabela...">
            <div class="input-group-append">
                <button v-on:click="setFilter()" class="btn btn-outline-secondary" type="button">Pesquisar</button>
            </div>
        </div>
      </div>

    </div>

    <div class="row mt-3">

      <div class="col">

        <div class="card">
        <div class="card-body">
            <!-- TABLE -->
            <vuetable 
                ref="vuetable" 
                api-url="/admin/membros/index.json" 
                :fields="fields" 
                :per-page="15"
                :css="css.table"
                :sort-order="sortOrder"
                :query-params="{ sort: 'sort', order: 'direction', page: 'page', perPage: 'limit' }"
                :append-params="moreParams"
                pagination-path="pagination" 
                @vuetable:checkbox-toggled="toggledChbox($event)"
                @vuetable:pagination-data="onPaginationData">

                <template slot="actions" scope="props">
                    <div class="text-right">
                        <button v-if="props.rowData.status == 0" class="btn btn-outline-success  btn-sm mr-1" @click="approveRow(props.rowData)">
                            Aprovar
                        </button>
                        <button class="btn btn-outline-secondary btn-sm mr-1" @click="viewRow(props.rowData)">
                            Consultar
                        </button>
                        <button class="btn btn-outline-primary btn-sm" @click="editRow(props.rowData)">
                            Editar
                        </button>
                    </div>
                </template>

                <template slot="member_link" scope="props">
                    <a href="#" @click="viewRow(props.rowData)">{{props.rowData.name}}</a>
                </template>

                <template slot="status" scope="props">
                    <div v-if="props.rowData.status == 0">
                        <div class="badge badge-warning">Inativo</div>
                    </div>
                    <div v-if="props.rowData.status == 1">
                        <div class="badge badge-success">Ativo</div>
                    </div>
                    <div v-if="props.rowData.status == 2">
                        <div class="badge badge-danger">Cancelado</div>
                    </div>
                </template>

            </vuetable>
        </div>
        </div>

        <div class="mt-2 vue_table_footer">

            <button class="btn btn-sm btn-outline-secondary mt-3 mb-4" v-on:click="deleteSelected">Eliminar selecionados</div>

            <vuetable-pagination 
                ref="pagination" 
                :css="css.pagination"
                @vuetable-pagination:change-page="onChangePage">
            </vuetable-pagination>

        </div>

      </div>
    </div>


  </div>
</div>

<script src="https://unpkg.com/moment@2.20.1"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.6/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.16.1/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/babel-preset-es2015@6.24.1/lib/index.min.js"></script>
<script src="https://unpkg.com/vuetable-2@1.6.0"></script>

<script>
Vue.use(Vuetable)

new Vue({
    el: '#vuejs_instance',
    components:{
        'vuetable-pagination': Vuetable.VuetablePagination
    },
    data: {
        flashmsg:'',
        filterText: '',
        filterTextData: '',
        filterTextType: 'name',
        moreParams: {},
        sortOrder: [
            { field: 'name', direction: 'asc' }
        ],
        fields: [
            '__checkbox',
            {
                name: 'num',
                title: 'Nº',
                sortField: 'num'
            },
            {
                name:'__slot:member_link',
                title: 'Membro'
            },
            {
                name: 'email',
                title: 'Email'
            },
            {
                name: '__slot:status',
                title: 'Estado',
                sortField: 'status'
            },
            {
                name: '__slot:actions',
                dataClass: 'text-center'
            }
        ],
        css: {
            table: {
                tableClass: 'table  table-hover',
                loadingClass: 'loading',
                ascendingIcon: 'fa fa-chevron-up',
                descendingIcon: 'fa fa-chevron-down',
                handleIcon: 'fa fa-menu-hamburger',
            },
            pagination: {
                wrapperClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                pageClass: 'page-item page-link',
                linkClass: 'page-link',
                paginationClass: 'pagination',
                paginationInfoClass: 'float-left',
                dropdownClass: 'form-control',
                icons: {
                    first: 'fa fa-chevron-left',
                    prev: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    last: 'fa fa-chevron-right',
                },
            }
        }
    },
    methods: {
        transform: function(d) {
            //alteramos o formato dos dados porque o Cake devolve com nomes
            //diferentes do necessario para o vue-table
            var transformed = {}

            transformed.pagination = {
                total: d.pagination.count,
                per_page: d.pagination.perPage,
                current_page: d.pagination.page,
                last_page: d.pagination.pageCount,
                next_page_url: d.pagination.nextPage,
                prev_page_url: d.pagination.prevPage
            }
            
            //temos de adicionar também os dados normais ao novo objecto
            transformed.data = d.data;

            return transformed
        },
        transformDate (value) {
            return moment(value).format("DD-MM-YYYY")
        },
        //https://ratiw.github.io/vuetable-2/#/Sorting?id=overriding-the-sort-query-string
        getSortParam: function(sortOrder) {
            //requer um *hack* no controller do cakephp
            return sortOrder.map(function(sort) {
                return sort.field+'#'+sort.direction
            }).join(',')
            
        },
        toggledChbox(payload, dataItem) {
            console.log('row: '+this.$refs.vuetable.selectedTo)
        },
        onPaginationData (paginationData) {
            this.$refs.pagination.setPaginationData(paginationData)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        editRow(rowData){
            window.location.href = '/admin/membros/edit/'+rowData.id
        },
        viewRow(rowData){
            window.location.href = '/admin/usrs/view/'+rowData.user_id+'#tab_member'
        },
        approveRow(rowData){
            var vThis = this;
            var vtable = this.$refs.vuetable;
            if(confirm('Pretende efetuar a aprovação de membro?')){
                axios.post('/admin/membros/approve.json', {
                    member_id: rowData.id
                })
                .then(function (response) {
                    vThis.flashmsg = 'Membro aprovado com sucesso. <a href="/admin/usrs/view/'+response.data.result.user_id+'">Consultar</a>'
                    vtable.refresh() 
                    console.log(response)
                    console.log(response.data)
                })
                .catch(function (error) {
                    console.log(error);
                });
                vtable.refresh()
            }
        },
        setFilter (filterText) {

            this.moreParams = {
                'filter': this.filterTextData,
                'filterType': this.fitlerTextType           
            }
            Vue.nextTick( () => this.$refs.vuetable.refresh())
        },
        deleteSelected(){
            var selectedRows = this.$refs.vuetable.selectedTo
            var vtable = this.$refs.vuetable;
            if(selectedRows.length > 0){
                if(confirm('Pretende eliminar o(s) item(s) selecionado(s)?')){
                    axios.post('/admin/membros/delete', {
                        ids: selectedRows
                    })
                    .then(function (response) {
                        vtable.refresh()
                        console.log('success: '+response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                    vtable.refresh()
                }
            }
        }
    }
})
</script>