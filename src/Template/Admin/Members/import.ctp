<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
       <h2>Importação de Membros</h2>
    </div>
  </div>
<?php $this->end(); ?>

<?=$this->Form->create('Import',['url'=>['controller'=>'members','action'=>'import'],'enctype' => 'multipart/form-data'])?>
<div class="row justify-content-md-center">
    <div class="col col-md-10">

        <div class="card">
            <div class="card-body">
            <div class="alert alert-info">
                <span>Selecione do seu computador um ficheiro *.csv válido para importação de membros. Efetue o download do exemplo e preencha os campos com os seus dados. <?=$this->Html->link('Download de ficheiro/guia de CSV válido', ['action'=>'csvdemo'],['class'=>'link']) ?></span>
            </div>

            <label>Upload de ficheiro</label> 
            <?php echo $this->Form->file('file') ?>
            <hr>
            <p><small><strong>Atenção:</strong> *.csv é o único formato de ficheiro válido.</small></p>

            

            </div>
        </div>

        <div class="box bg-white">
            <?=$this->Form->button('<i class="fa fa-fw fa-fw fa-save"></i> '.__('Validar ficheiro de importação'),['type'=>'submit','class'=>'btn btn-success','escape'=>false]);?>
            <div class="clearfix"></div>
        </div>

    </div>
</div>
<?=$this->Form->end()?>