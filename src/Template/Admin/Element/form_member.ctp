<link rel="stylesheet" type="text/css" media="print" href="/getcode_cms/css/print.css">

<?= $this->Html->css([
  '//cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css'
])?>

<?= $this->Html->script([
  '//cdn.jsdelivr.net/npm/flatpickr',
  '//unpkg.com/flatpickr@4.5.1/dist/l10n/pt.js'
])?>


<?php use Cake\Core\Configure; ?>

<?=$this->Form->create($member)?>


<div id="inline_collumn" class="row justify-content-md-center">
  <div class="col col-md-10">

    <ul class="nav nav-tabs" id="posts_tabs" role="tablist">
      <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab_editor" aria-selected="true">Membro</a></li>
    </ul>

    <div class="tab-content" data-tabs-content="posts_tabs">

      <div class="card tab-pane fade show active" id="tab_editor">
        <div class="card-body">
  
          

              <div class="row">
                <div class="form-group col">
                  <?= $this->Form->control('num', ['label'=>'Nº Membro/Sócio','class'=>'form-control']); ?>
                  <small class="form-text text-muted">
                    Ultimo numero de membro inserido: <strong><?= $last_member_number ?></strong>
                  </small>
                  
                </div>
                <div class="form-group col">
                  <?= $this->Form->control('name', ['label'=>'Nome','class'=>'form-control']); ?>
                  <small class="form-text text-muted">
                    Nome do membro
                  </small>
                </div>

                <div class="form-group col">
                  <?= $this->Form->control('email', ['label'=>'Email','class'=>'form-control']); ?>
                  <small class="form-text text-muted">
                    Introduza email válido para contactos
                  </small>
                </div>

              </div>
              <div class="row">
                <div class="form-group col">
                  <?= $this->Form->control('idnumber', ['label'=>'Nº de Identificção','class'=>'form-control']); ?>
                  <small class="form-text text-muted">
                    Documento de identificação (ex.: CC, Passaporte, etc.,...)
                  </small>
                </div>
                <div class="form-group col">
                  <?= $this->Form->control('birthdate', ['label'=>'Data de nascimento','class'=>'form-control flatpicker', 'type'=>'text']); ?>
                  <small class="form-text text-muted">
                    Data de nascimento do membro
                  </small>
                </div>
                <div class="form-group col">
                  <?= $this->Form->control('phone', ['label'=>'Telefone','class'=>'form-control']); ?>
                  <small class="form-text text-muted">
                    Numero de contacto válido
                  </small>
                </div>
                
              </div>
              <div class="row">
                <div class="form-group col-6">
                  <?= $this->Form->control('address', ['label'=>'Morada','class'=>'form-control']); ?>
                </div>
                <div class="form-group col">
                  <?= $this->Form->control('local', ['label'=>'Localidade','class'=>'form-control']); ?>
                </div>
                <div class="form-group col">
                  <?= $this->Form->control('zip', ['label'=>'Código Postal','class'=>'form-control']); ?>
                </div>
              </div>
              <div class="row">
                <div class="form-group col">
                  <?= $this->Form->control('admission_date', ['label'=>'Data de admissão','class'=>'form-control flatpicker', 'type'=>'text']); ?>
                  <small class="form-text text-muted">
                    Data de registo/admissão do membro
                  </small>
                </div>
                <div class="form-group col">
                  <?= $this->Form->control('status', ['label'=>'Estado','options' => Configure::read('member_status'), 'class'=>'form-control']); ?>
                  <small class="form-text text-muted">
                    Indica o estado do membro/sócio na plataforma
                  </small>
                </div>
                <div class="form-group col-md-4 d-print-none">
                  <label>Utilizador associado</label>
                  <?= $this->Form->select('user_id', $users, ['class'=>'form-control select2']); ?>
                  <small class="form-text text-muted">
                    Indica o estado do membro/sócio na plataforma
                  </small>
                </div>

              </div>
 
        
        </div>
      </div>

    </div>

    <div class="box bg-white">

      <?=$this->Form->button('<i class="fa fa-fw fa-fw fa-save"></i> '.__('Gravar'),['type'=>'submit','class'=>'btn btn-md btn-success','escape'=>false]);?>
      <?=$this->Html->link('<i class="fa fa-fw fa-fw fa-undo"></i> '.__('Cancelar'),['controller'=>'members','action'=>'index'],['class'=>'btn btn-outline-secondary','escape'=>false]);?>

      <?php if ($this->request->params['action'] == 'edit'): ?>
        <?=$this->Form->postLink('<i class="fa fa-fw fa-trash-o"></i> '.__('Remover'),['action'=>'delete',$member->id],['block' => true,'class'=>'pull-right btn btn-sm btn-outline-danger','confirm'=>'Tem a certeza que deseja remover este item?','escape'=>false]);?>
      <?php endif; ?>
     
      <div class="clearfix"></div>
    </div>

  </div>

</div>

<?=$this->Form->end()?>
<?php
//fetch delete form because it's inside the main form and can't be nested.
echo $this->fetch('postLink');
?>

<?= $this->Html->css(['/GetcodeCms/vendor/select2/dist/css/select2.min.css'])?>
<?= $this->Html->script(['/GetcodeCms/vendor/select2/dist/js/select2.min.js'])?>

<script type="text/javascript">

$('.select2').select2({
  tags: true
});

$(".flatpicker").flatpickr({
  "locale": "pt"
});

</script>