<?php use Cake\Core\Configure; ?>

<?= $this->Html->css([
  '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css',
  '//cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css'
])?>
<?= $this->Html->script([
  '//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js',
  '//cdn.jsdelivr.net/npm/flatpickr',
  '//unpkg.com/flatpickr@4.5.1/dist/l10n/pt.js'
])?>

<?=$this->Form->create($subscription)?>

<?php echo $this->fetch('form_topnav'); ?>

<div class="row justify-content-md-center">

  <div class="col col-md-10">

    <ul class="nav nav-tabs" id="posts_tabs" role="tablist">
      <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab_editor" aria-selected="true">Subscrição</a></li>
    </ul>

    <div class="tab-content" data-tabs-content="posts_tabs">

      <div class="card tab-pane fade show active" id="tab_editor">
        <div class="card-body">

          <div class="row">

            <div class="col col-md-12">
              <div class="alert alert-info">
                <span>A subscrição tem associado um membro e um plano. Pode ser colocada uma data de expiração.</span>
              </div>
            </div>
            
            <div class="form-group col-md-4">
              <?= $this->Form->control('member_id', ['label'=>'Membro','options' => $members, 'class'=>'select2 form-control']); ?>
              <small class="form-text text-muted">
                Membro a quem pertence esta subscrição.
              </small>
            </div>

            <div class="form-group col-md-5">
              <?= $this->Form->control('plan_id', ['label'=>'Produto','class'=>'form-control','empty'=>'Escolher...']); ?>
              <small class="form-text text-muted">
                Item associado a esta subscrição (ex: Quota, Aulas, etc)
              </small>
            </div>

            <div class="form-group col-md-3">
              <label>Estado</label>
              <?= $this->Form->select('status', Configure::read('subscriptions_status'), ['class'=>'form-control', 'default'=>1]); ?>
            </div>

          </div>

          <div class="row">

            <div class="form-group col">
              <label>Valor</label>
              <div class="input-group">
                <?php $disabled = $this->request->params['action'] == 'edit' ? true : false ; ?>
                <?= $this->Form->number('amount', ['class'=>'form-control', 'disabled' => $disabled]); ?>
                <div class="input-group-append">
                    <span class="input-group-text">€</span>
                </div>
              </div>
            </div>

            <div class="form-group col">
              <label>Método de pagamento pré-definido</label>
              <?= $this->Form->select('method_id', $methods, ['class'=>'form-control','default'=>2]); ?>
            </div>



            <div class="form-group col">
              <?= $this->Form->control('expires_at', ['label'=>'Expiração do ciclo','class'=>'form-control flatpicker', 'type'=>'text']); ?>
              <small class="form-text text-muted">
                As repetições duram até esta data. Caso não exista é considerado contínuo.
              </small>
            </div>

            <div class="form-group col">
              <?= $this->Form->control('activated_at', ['value'=>date('Y-m-d'),'label'=>'Início da subscrição','class'=>'form-control flatpicker', 'type'=>'text']); ?>
              <small class="form-text text-muted">
                Data respetiva ao início da subscrição
              </small>
            </div>

          </div>

        </div>
      </div>

    </div>

    <div class="box bg-white">
      <?=$this->Form->button('<i class="fa fa-fw fa-fw fa-save"></i> '.__('Gravar'),['type'=>'submit','class'=>'btn btn-success','escape'=>false]);?>
      <?=$this->Html->link('<i class="fa fa-fw fa-fw fa-undo"></i> '.__('Cancelar'),['controller'=>'subscriptions','action'=>'index'],['class'=>'btn btn-outline-secondary','escape'=>false]);?>

      <?php if ($this->request->params['action'] == 'edit'): ?>
        <?=$this->Form->postLink('<i class="fa fa-fw fa-trash-o"></i> '.__('Remover'),['action'=>'delete',$subscription->id],['block' => true,'class'=>'btn btn-sm btn-outline-danger pull-right','confirm'=>'Tem a certeza que deseja remover este item?','escape'=>false]);?>
      <?php endif; ?>

      <div class="clearfix"></div>
    </div>

  </div>

</div>

<?=$this->Form->end()?>

<?php
//fetch delete form because it's inside the main form and can't be nested.
echo $this->fetch('postLink');
?>

<script type="text/javascript">

  $('.select2').select2({
    tags: true
  });

  $(".flatpicker").flatpickr({
    "locale": "pt"
  });

</script>