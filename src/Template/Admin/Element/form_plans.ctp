<?php use Cake\Core\Configure; ?>

<?=$this->Form->create($plan)?>

<div class="row justify-content-md-center">

  <div class="col col-md-10">

    <ul class="nav nav-tabs" id="posts_tabs" role="tablist">
      <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab_editor" aria-selected="true">Detalhe</a></li>
    </ul>

    <div class="tab-content" data-tabs-content="posts_tabs">

      <div class="card tab-pane fade show active" id="tab_editor">
        <div class="card-body">

            <div class="row">
              <div class="form-group col-6">
                <?= $this->Form->control('name', ['label'=>'Nome','class'=>'form-control']); ?>
                <small class="form-text text-muted">
                  Nome do produto
                </small>
              </div>
              <div class="form-group col-3">
                <?= $this->Form->control('code', ['label'=>'Código','class'=>'form-control']); ?>
                <small class="form-text text-muted">
                  Código do produto
                </small>
              </div>
              <div class="form-group col-3">
                <label>Estado</label>
                <?= $this->Form->select('status', Configure::read('plan_status'),['class'=>'form-control']); ?>
                <small class="form-text text-muted">
                  Descrição do produto
                </small>
              </div>

              <div class="form-group col-md-4">
                <label>Duração do ciclo</label>
                <div class="input-group">
                  <?= $this->Form->number('interval_num', ['label'=>'Cobrado a cada','class'=>'form-control']); ?>
                  <div class="input-group-append">
                    <?= $this->Form->select('interval_unit', Configure::read('interval_unit'), ['default'=>'month', 'id'=>'interval_unit', 'class'=>'form-control']); ?>
                    <?= $this->Form->input('interval_unit_formatted', ['id'=>'interval_unit_formatted','type'=>'hidden','default'=>'month'])?>
                  </div>
                  <small class="form-text text-muted">
                  O ciclo gera uma ordem de pagamento consoante a duração do mesmo.
                  </small>
                </div>
              </div>

              <div class="form-group col-12">
                <?= $this->Form->control('description', ['label'=>'Descrição','class'=>'form-control']); ?>
                <small class="form-text text-muted">
                  Descrição do produto
                </small>
              </div>
            </div>
        </div>
      </div>

    </div>
    
    <div class="box bg-white">

      <?=$this->Form->button('<i class="fa fa-fw fa-fw fa-save"></i> '.__('Gravar'),['type'=>'submit','class'=>'btn btn-md btn-success','escape'=>false]);?>
      <?=$this->Html->link('<i class="fa fa-fw fa-fw fa-undo"></i> '.__('Cancelar'),['controller'=>'plans','action'=>'index'],['class'=>'btn btn-outline-secondary','escape'=>false]);?>

      <?php if ($this->request->params['action'] == 'edit'): ?>
        <?=$this->Form->postLink('<i class="fa fa-fw fa-trash-o"></i> '.__('Remover'),['action'=>'delete',$plan->id],['block' => true,'class'=>'pull-right btn btn-sm btn-outline-danger','confirm'=>'Tem a certeza que deseja remover este item?','escape'=>false]);?>
      <?php endif; ?>
     
      <div class="clearfix"></div>
      
    </div>

  </div>

</div>

<?=$this->Form->end()?>
<?php
//fetch delete form because it's inside the main form and can't be nested.
echo $this->fetch('postLink');
?>

<?= $this->Html->css([
  '//cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.css',
  '//cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.date.css',
  '//cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/themes/default.time.css',
  '/GetcodeCms/vendor/select2/dist/css/select2.min.css',
  ])?>
<?= $this->Html->script([
  '//cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.js',
  '//cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.date.js',
  '//cdnjs.cloudflare.com/ajax/libs/pickadate.js/3.5.6/compressed/picker.time.js',
  '/GetcodeCms/vendor/select2/dist/js/select2.min.js',
])?>

<script type="text/javascript">

var dateOpts = {
  format: 'yyyy-mm-dd',
  monthsFull: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
  monthsShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
  weekdaysFull: ['Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado'],
  weekdaysShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sab'],
  today: 'Hoje',
  clear: 'Limpar',
  close: 'Fechar'
};
  
$('.datepicker').pickadate(dateOpts);

$('.select2').select2({
  tags: true
});


$('#interval_unit').on('change', function() {
  $('#interval_unit_formatted').val(this.value);
})
</script>