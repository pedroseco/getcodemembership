<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
      <h2>Subscrições</h2>
    </div>
  </div>
<?php $this->end(); ?>

<div class="row justify-content-md-center" id="vuejs_instance">
  <div class="col col-md-10">

    <div class="row justify-content-between">

      <div class="col col-md-4">

          <?=$this->Html->link('ADICIONAR',
            ['controller'=>'subscriptions','action'=>'add'],
            ['class'=>'btn btn-md btn-primary'])
          ?>
          <a href="/admin/subscricoes/export.csv" class="btn btn-md btn-outline-secondary">EXPORTAR CSV</a>
 

      </div>

      <div class="col col-md-4 align-self-end">
        <div class="input-group">
            <input v-model="filterTextData" type="search" class="form-control" placeholder="Pesquisa na tabela...">
            <div class="input-group-append">
                <button v-on:click="setFilter()" class="btn btn-outline-secondary" type="button">Pesquisar</button>
            </div>
        </div>
      </div>

    </div>

    <div class="row mt-3">
      <div class="col">

        <div class="card car">
        <div class="card-body">
            <!-- TABLE -->
            <vuetable 
                ref="vuetable" 
                api-url="/admin/subscricoes/index.json" 
                :fields="fields" 
                :per-page="5"
                :css="css.table"
                :sort-order="sortOrder"
                :query-params="{ sort: 'sort', order: 'direction', page: 'page', perPage: 'limit' }"
                :append-params="moreParams"
                pagination-path="pagination" 
                @vuetable:checkbox-toggled="toggledChbox($event)"
                @vuetable:pagination-data="onPaginationData">

                <template slot="actions" scope="props">
                    <div class="text-right">
                        <button class="btn btn-outline-primary btn-sm" @click="editRow(props.rowData)">
                            Editar
                        </button>
                    </div>
                </template>

                <template slot="current_term_starts_at" scope="props">
                    {{props.rowData.current_term_starts_at | moment}}
                </template>

                <template slot="amount" scope="props">
                    {{props.rowData.amount}}€
                </template>

            </vuetable>

        </div>
        </div>

        <div class="mt-2 vue_table_footer">

            <button class="btn btn-sm btn-outline-secondary mt-3 mb-4" v-on:click="deleteSelected">Eliminar selecionados</div>

            <vuetable-pagination 
                ref="pagination" 
                :css="css.pagination"
                @vuetable-pagination:change-page="onChangePage">
            </vuetable-pagination>

        </div>
        

      </div>
    </div>


  </div>
</div>


<script>
Vue.use(Vuetable)

new Vue({
    el: '#vuejs_instance',
    components:{
        'vuetable-pagination': Vuetable.VuetablePagination
    },
    data: {
        filterTextData: '',
        moreParams: {},
        sortOrder: [
            { field: 'name', direction: 'asc' }
        ],
        fields: [
            '__checkbox',
            {
                name: 'member',
                title: 'Membro/Sócio',
                callback: 'transformMember'
            },
            {
                name: 'plan.name',
                title: 'Produto',
                sortField: 'name'
            },
            {
                name: '__slot:current_term_starts_at',
                title: 'Data ativação',
                sortField: 'current_term_starts_at'
            },
            {
                name: 'next_billing_at',
                title: 'Próx. Pagamento',
                sortField: 'next_billing_at'
            },
            {
                name: '__slot:amount',
                title: 'Valor',
                sortField: 'amount'
            },
            {
                name: 'status',
                title: 'Estado',
                sortField: 'status',
                callback: 'transformStatus'
            },
            '__slot:actions'
        ],
        css: {
            table: {
                tableClass: 'table  table-hover',
                loadingClass: 'loading',
                ascendingIcon: 'fa fa-chevron-up',
                descendingIcon: 'fa fa-chevron-down',
                handleIcon: 'fa fa-menu-hamburger',
            },
            pagination: {
                infoClass: 'pull-left',
                wrapperClass: 'vuetable-pagination pull-right',
                activeClass: 'btn btn-outline-default',
                disabledClass: 'disabled',
                pageClass: 'btn btn-border',
                linkClass: 'btn btn-border',
                icons: {
                    first: '',
                    prev: '',
                    next: '',
                    last: '',
                },
            }
        }
    },
    filters: {
      moment: function (date) {
        return moment(date).format('YYYY-MM-DD')
      },
      moment_from: function (date) {
        return moment(date, 'YYYY-MM-DD').fromNow()
      },
      moment_time: function (time) {
        return moment(time).format('H:mm')
      }
    },
    methods: {
        transform: function(d) {
            //alteramos o formato dos dados porque o Cake devolve com nomes
            //diferentes do necessario para o vue-table
            var transformed = {}

            transformed.pagination = {
                total: d.pagination.count,
                per_page: d.pagination.perPage,
                current_page: d.pagination.page,
                last_page: d.pagination.pageCount,
                next_page_url: d.pagination.nextPage,
                prev_page_url: d.pagination.prevPage
            }
            
            //temos de adicionar também os dados normais ao novo objecto
            transformed.data = d.data;

            return transformed
        },
        transformMember (value) {
            console.log(value);
            return '<a href="/admin/usrs/view/'+value.user_id+'#tab_member"><strong>'+value.name+'</strong><br>'+value.email+'</a>'
        },
        transformStatus (value) {
            return value==1?'<span class="badge badge-primary">Ativo</span>':'<span class="badge badge-warning">Inativo</span>'
        },
        //https://ratiw.github.io/vuetable-2/#/Sorting?id=overriding-the-sort-query-string
        getSortParam: function(sortOrder) {
            //requer um *hack* no controller do cakephp
            return sortOrder.map(function(sort) {
                return sort.field+'#'+sort.direction
            }).join(',')
            
        },
        toggledChbox(payload, dataItem) {
            console.log('row: '+this.$refs.vuetable.selectedTo)
        },
        onPaginationData (paginationData) {
            this.$refs.pagination.setPaginationData(paginationData)
        },
        onChangePage (page) {
            this.$refs.vuetable.changePage(page)
        },
        editRow(rowData){
            console.log(rowData);
            window.location.href = '/admin/subscricoes/edit/'+rowData.id
        },
        setFilter () {
            this.moreParams.filter = this.filterTextData
            Vue.nextTick( () => this.$refs.vuetable.refresh())
        },
        deleteSelected(){
            var selectedRows = this.$refs.vuetable.selectedTo
            var vtable = this.$refs.vuetable;
            if(selectedRows.length > 0){
                if(confirm('Pretende eliminar o(s) item(s) selecionado(s)?')){
                    axios.post('/admin/subscricoes/delete', {
                        ids: selectedRows
                    })
                    .then(function (response) {
                        vtable.refresh()
                        console.log('success: '+response);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                    vtable.refresh()
                }
            }
        }
    }
})
</script>