<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
        <h2>Adicionar Subscrição</h2>
    </div>
  </div>
<?php $this->end(); ?>

<div class="row">
    <div class="col">
        <?php echo $this->Flash->render(); ?>
    </div>
</div>

<?php
echo $this->element('form_subscriptions');
?>