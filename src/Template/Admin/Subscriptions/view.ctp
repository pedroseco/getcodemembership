<?php use Cake\Core\Configure; ?>
<?php $this->append('top_bar'); ?>
  <div class="row justify-content-md-center">
    <div class="col col-md-10">
        <h2>Consultar Subscrição</h2>
    </div>
  </div>
<?php $this->end(); ?>

<div class="row justify-content-md-center">

  <div class="col col-md-10">

    <ul class="nav nav-tabs" id="posts_tabs" role="tablist">
      <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#tab_editor" aria-selected="true">Subscrição</a></li>
    </ul>

    <div class="tab-content" data-tabs-content="posts_tabs">

      <div class="card tab-pane fade show active" id="tab_editor">
        <div class="card-body">

            <div class="subscriptions view large-9 medium-8 columns content">
                <h3><?= h($subscription->name) ?></h3>
                <table class="vertical-table">
                    <tr>
                        <th scope="row"><?= __('Member') ?></th>
                        <td><?= $subscription->has('member') ? $this->Html->link($subscription->member->name, ['controller' => 'Members', 'action' => 'view', $subscription->member->id]) : '' ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Id') ?></th>
                        <td><?= $this->Number->format($subscription->id) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Status') ?></th>
                        <td><?= $this->Number->format($subscription->status) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Sub Total') ?></th>
                        <td><?= $this->Number->format($subscription->sub_total) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Amount') ?></th>
                        <td><?= $this->Number->format($subscription->amount) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Interval') ?></th>
                        <td><?= $this->Number->format($subscription->interval) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Plan Id') ?></th>
                        <td><?= $this->Number->format($subscription->plan_id) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Current Term Start At') ?></th>
                        <td><?= h($subscription->current_term_start_at) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Current Terms Ends At') ?></th>
                        <td><?= h($subscription->current_terms_ends_at) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Last Billing At') ?></th>
                        <td><?= h($subscription->last_billing_at) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Next Billing At') ?></th>
                        <td><?= h($subscription->next_billing_at) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Created') ?></th>
                        <td><?= h($subscription->created) ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?= __('Modified') ?></th>
                        <td><?= h($subscription->modified) ?></td>
                    </tr>
                </table>
            </div>

        </div>
      </div>
    </div>
  </div>
</div>



