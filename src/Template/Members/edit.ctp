<?php use Cake\Core\Configure; ?>

<?= $this->Html->css([
  '//cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css'
])?>

<?= $this->Html->script([
  '//cdn.jsdelivr.net/npm/flatpickr',
  '//unpkg.com/flatpickr@4.5.1/dist/l10n/pt.js'
])?>

<div class="container">
    <?php if(!empty($this->request->Session()->read('Auth.User.id'))):?>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary rounded mb-4">
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard#tab_geral">Dashboard <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard#tab_invoices">Documentos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard#tab_eventos">Eventos</a>
                    </li>        
                </ul>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <?= $this->Html->link('Sair <i class="fa fa-fw fa-sign-out"></i>', ['controller'=>'users','action'=>'logout','plugin'=>'CakeDC/Users'],['class'=>'nav-link','escape'=>false])?>
                    </li>
                </ul>
            </div>
        </nav>
    <?php endif;?>
</div>

<div class="dashboard-wrapper">
    <div class="container">

        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Área pessoal</span>
            <h3 class="page-title">Editar perfil</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom text-center">
                        <h4 class="mt-3 mb-0"><?php echo $member->name ?></h4>
                        <span class="text-muted d-block mb-3"><small>Membro desde: <?php echo $member->created->format('d-m-Y') ?></small></span>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-4">
                            <strong class="text-muted d-block mb-2">Detalhes</strong>
                            <?= !empty($member->phone) ? '<span><strong>Telefone:</strong> '.h($member->phone).'</span><br>' :''; ?> 
                            <?= !empty($member->email) ? '<span><strong>Email:</strong> '.h($member->email).'</span><br>' :''; ?> 
                            <?= !empty($member->created) ? '<span><strong>Registo criado:</strong> '.h($member->created->i18nFormat("dd-MM-yyyy")).'</span><br>' :'';?> 
                            <?= !empty($member->modified) ? '<span><strong>Registo modificado:</strong> '.h($member->modified->i18nFormat("dd-MM-yyyy")).'</span><br>' :'';?>
                        </li>
                        <li class="list-group-item p-4">
                            <strong class="text-muted d-block mb-2">Morada</strong>
                            <?= !empty($member->address) ? '<strong>Rua:</strong> <span>'.h($member->address).'</span><br/>' :'';?> 
                            <?= !empty($member->local) ? '<strong>Localidade:</strong> <span>'.h($member->local).'</span>' :'';?>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card card-small mb-4">

                    <?=$this->Form->create($member)?>

                        <div class="card-header border-bottom"><h6 class="m-0">Informação</h6></div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item p-3">
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('num', ['label'=>'Nº Membro/Sócio','class'=>'form-control', 'disabled'=>true]); ?>
                                            </div>
                                        </div>
                                        <div class="row">

                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('name', ['label'=>'Nome','class'=>'form-control']); ?>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('email', ['label'=>'Email','class'=>'form-control']); ?>
                                            </div>
                                            
                                            <div class="clearfix"></div>

                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('idnumber', ['label'=>'Nº de Identificção','class'=>'form-control']); ?>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('birthdate', ['label'=>'Data de nascimento','class'=>'form-control flatpicker', 'type'=>'text']); ?>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('phone', ['label'=>'Telefone','class'=>'form-control']); ?>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('address', ['label'=>'Morada','class'=>'form-control']); ?>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <?= $this->Form->control('local', ['label'=>'Localidade','class'=>'form-control']); ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <div class="card-footer">
                            <?=$this->Form->button('<i class="fa fa-fw fa-fw fa-save"></i> '.__('Gravar alterações'),['type'=>'submit','class'=>'btn btn-sm btn-success','escape'=>false]);?>
                            <?=$this->Html->link(__('Cancelar'),$this->request->referer(),['class'=>'btn btn-sm btn-light','escape'=>false]);?>
                        </div>

                    <?=$this->Form->end()?>

                </div>
            </div>
        </div>
    
    </div>
</div>

<script>
    $(".flatpicker").flatpickr({
        "locale": "pt"
    });
</script>