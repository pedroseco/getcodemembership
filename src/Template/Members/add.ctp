<?php use Cake\Core\Configure; ?>

<div class="container">
    <?php echo $this->Flash->render() ?>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php echo $this->Form->create('Member', ['url'=>['controller'=>'Members', 'action'=>'add', 'plugin'=>'GetcodeMembership', 'prefix'=>null]])?>
                
                <div class="form-group">
                    <?php echo $this->Form->control('name', ['class'=>'form-control', 'label'=>'Nome completo']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('idnumber', ['class'=>'form-control', 'label'=>'Nº Identificação (CC, BI, Passaporte, etc...)']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('birthdate', ['class'=>'form-control datepicker', 'label'=>'Data de Nascimento']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('gender', ['class'=>'form-control', 'label'=>'Sexo', 'options'=>Configure::read('gender')]);?>
                </div>                     
                <div class="form-group">
                    <?php echo $this->Form->control('address', ['class'=>'form-control', 'label'=>'Morada']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('local', ['class'=>'form-control', 'label'=>'Localidade']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('phone', ['class'=>'form-control', 'label'=>'Contacto telefónico']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('email', ['class'=>'form-control', 'label'=>'Email']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('password', ['class'=>'form-control', 'label'=>'Password']);?>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->control('password_', ['class'=>'form-control', 'label'=>'Repetir Password']);?>
                </div>
                <div class="form-group">
                    <label>Subscrição</label>
                    <?php echo $this->Form->select('subscriptions', $plans, ['class'=>'form-control']);?>
                </div>

                <?php echo $this->Form->button('ENVIAR', ['type'=>'submit','class'=>'btn btn-default btn-action btn-block'])?>
            
            <?php echo $this->Form->end();?>
        </div>
    </div>
</div>

<script>
    $( ".datepicker" ).datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: "1920:<?php echo date('Y')?>",
    });
</script>