<?php
namespace GetcodeMembership\Model\Entity;

use Cake\ORM\Entity;

/**
 * Plan Entity
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property string $description
 * @property bool $status
 * @property int $product_id
 * @property int $tax_percentage
 * @property string $tax_name
 * @property int $trial_period
 * @property float $setup_fee
 * @property float $recurring_price
 * @property string $url
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \GetcodeMembership\Model\Entity\Product $product
 */
class Plan extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
