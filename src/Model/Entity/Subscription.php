<?php
namespace GetcodeMembership\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subscription Entity
 *
 * @property int $id
 * @property string $name
 * @property int $status
 * @property float $sub_total
 * @property float $amount
 * @property \Cake\I18n\FrozenTime $current_term_start_at
 * @property \Cake\I18n\FrozenTime $current_terms_ends_at
 * @property \Cake\I18n\FrozenTime $last_billing_at
 * @property \Cake\I18n\FrozenTime $next_billing_at
 * @property int $interval
 * @property string $interval_unit
 * @property int $plan_id
 * @property int $member_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \GetcodeMembership\Model\Entity\Plan $plan
 * @property \GetcodeMembership\Model\Entity\Member $member
 */
class Subscription extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
