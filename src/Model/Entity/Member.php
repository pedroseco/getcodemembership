<?php
namespace GetcodeMembership\Model\Entity;

use Cake\ORM\Entity;

/**
 * Member Entity
 *
 * @property int $id
 * @property int $num
 * @property string $name
 * @property int $idnumber
 * @property \Cake\I18n\FrozenDate $birthdate
 * @property string $local
 * @property string $address
 * @property int $phone
 * @property string $email
 * @property \Cake\I18n\FrozenDate $admission_date
 * @property bool $status
 * @property \Cake\I18n\FrozenDate $last_payment_date
 * @property float $last_payment_value
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Member extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
