<?php
namespace GetcodeMembership\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
/**
 * Subscriptions Model
 *
 * @property \GetcodeMembership\Model\Table\PlansTable|\Cake\ORM\Association\BelongsTo $Plans
 * @property \GetcodeMembership\Model\Table\MembersTable|\Cake\ORM\Association\BelongsTo $Members
 *
 * @method \GetcodeMembership\Model\Entity\Subscription get($primaryKey, $options = [])
 * @method \GetcodeMembership\Model\Entity\Subscription newEntity($data = null, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Subscription[] newEntities(array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Subscription|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \GetcodeMembership\Model\Entity\Subscription patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Subscription[] patchEntities($entities, array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Subscription findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SubscriptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('gc_subscriptions');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Plans', [
            'foreignKey' => 'plan_id',
            'joinType' => 'INNER',
            'className' => 'GetcodeMembership.Plans'
        ]);
        $this->belongsTo('Members', [
            'foreignKey' => 'member_id',
            'joinType' => 'INNER',
            'className' => 'GetcodeMembership.Members'
        ]);

        $this->hasMany('Invoiceitems', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER',
            'conditions'=> ['Invoiceitems.item_model'=>'Subscriptions'],
            'className' => 'GetcodePayments.Invoiceitems'
        ]);

        $this->hasMany('Invoices', [
            'foreignKey' => 'foreign_key',
            'joinType' => 'INNER',
            'className' => 'GetcodePayments.Invoices'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');


        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');


        $validator
            ->decimal('amount')
            ->requirePresence('amount', 'create')
            ->notEmpty('amount');

        $validator
            //->dateTime('expires_at')
            ->date( 'datetime' , 'ymd' , null)
            ->allowEmpty('expires_at');            

        $validator
            ->date( 'datetime' , 'ymd' , null)
            ->requirePresence('activated_at', 'create')
            ->notEmpty('activated_at');

        $validator
            ->dateTime('last_billing_at')
            ->allowEmpty('last_billing_at');

        $validator
            ->dateTime('next_billing_at')
            ->allowEmpty('next_billing_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['plan_id'], 'Plans'));
        $rules->add($rules->existsIn(['member_id'], 'Members'));

        return $rules;
    }


    /**
     * Gera um invoice após criar com sucesso 
     * os dados de uma nova Subscription
     * Valida a presença do plugin de pagamentos
     * @param object subscription
     */
    public function generateInvoiceAfterCreate($subscription)
    {
     
        //Procurar por membro associado.
        if(!empty($subscription->member_id)){
            $tableMembers = TableRegistry::get('GetcodeMembership.Members');
            $member = $tableMembers->get($subscription->member_id);

            $user_data = [
                'id' => !empty($member->user_id)?$member->user_id:0,
                'name' => !empty($member->name)?$member->name:'inválido',
                'email' => !empty($member->email)?$member->email:'inválido'
            ];

        }

        $sub = $this->get($subscription->id, ['contain'=>['Plans']]);


        $plan_name = $sub->plan->name.' | ('.date('Y-m-d', strtotime($sub->current_term_starts_at)).' - '.date('Y-m-d', strtotime($sub->current_term_ends_at)).')';



        //Prepara items para doCreateInvoice()
        $items[] = [
            'id'            => $sub->id,
            'price'         => $sub->amount,
            'description'   => $plan_name,
            'product_id'    => $sub->plan_id,
            'title'         => $sub->name,
            'item_model'    => 'Subscriptions',
            'tax_percentage'=> 23,
            'quantity'      => 1
        ];
       
        //template
        $template = null;
        $due_date = null;


        $invoices_table = Tableregistry::get('GetcodePayments.Invoices');
        $invoice = $invoices_table->doCreateInvoice($items,$user_data,'Subscriptions',$sub->id,$due_date,$template);

        $inv = Tableregistry::get('GetcodePayments.Invoices')->get($invoice->id, ['contain'=>['Invoiceitems']]);

        //criar uma transação apos criação do invoice
        //TODO: rever como será atribuido o metodo de pagamento a ser atribuido a uma subscricao??
        $payment_method = TableRegistry::get('GetcodePayments.Methods')->get(2);
        $transaction = TableRegistry::get('GetcodePayments.Transactions')->doTransactionCreate($inv,$payment_method);

        return $invoice;
    }


    /**
     * Valida as subscrições disponíveis e ativas
     * procurando pelas datas de current_term_ends_at para criar invoices e alterar 
     * os estados respetivos
     * 
     * @param date $target_date | Data (Y-m-d) a usar para filtro.
     * @return Object
     */
    public function findSubscriptionsInEndOfTerm($target_date)
    {
        //Procurar todas as Subscrições Ativas
        //onde a data de fim é a de Hoje
        $subs=$this->find()
            //->contain(['Members','Invoices'])
            ->where([
                'Subscriptions.status'=>1,
                'Subscriptions.current_term_ends_at <=' => date('Y-m-d',strtotime($target_date))
                ])
            ->select([
                'Subscriptions.id',
                'Subscriptions.status',
                'Subscriptions.amount',
                'Subscriptions.plan_id',
                'Subscriptions.member_id',
                'Subscriptions.current_term_starts_at',
                'Subscriptions.current_term_ends_at',
                'Subscriptions.next_billing_at',
                'Subscriptions.expires_at'
            ])
            ->all();
            
    
        
        return $subs;
    }

    /**
     * Recebe um objecto com um conjunto de subscrições já filtradas
     * e valida cada uma criando um invoice, caso não tenha sido criado.
     *
     * @param object $subscriptions resultado de um find()
     * @param date $target_date | Data (Y-m-d) a usar para filtro.
     * @param string $invoice | se true cria o Invoice
     * @return void
     */
    public function updateSubscriptionsInEndOfTerm($subscriptions,$target_date=null,$invoice = true)
    {

        //set do total de linhas a serem modificadas
        $result_rows=[];

        //Loop pelos resultados
       // if(!$subscriptions->isEmpty()):
   
            foreach($subscriptions as $s):



                //Gera um novo invoice se ainda não foi gerado
                //e se a renovação é infinita ou a data de fim ainda não foi atingida
                if(is_null($s->expires_at)):
                    
                    //Devolve o "Plano" a que pertence a subscrição para obtermos o período
                    //durante a qual a mesma ocorre.
                    if(!isset($s->plan) && !empty($s->plan_id)){
                        $plan=$this->Plans->get($s->plan_id);
                        $interval_num  = $plan->interval_num;
                        $interval_unit = $plan->interval_unit;
                    }else{
                        $interval_num  = $s->plan->interval_num;
                        $interval_unit = $s->plan->interval_unit;
                    }
                   
                    if($invoice == true){
                        $inv=$this->generateInvoiceAfterCreate($s);
                    }

                    
                    //A data de inicio do termo é igual á data do fim da anterior
                    $s->current_term_starts_at = $s->current_term_ends_at;

                    //Alteramos a data de renovação consoante o interval_unit e interval_num
                    $date_term_ends = $this->calculateEndOfTermDate($s->current_term_starts_at, $interval_num, $interval_unit);

                    $s->current_term_ends_at = $date_term_ends;
                    
                
                
                    if($this->save($s)){
                        
                         if(empty($target_date)){
                            $target_date = date('Y-m-d');
                         }

                        if($s->current_term_ends_at <= $target_date){
                            //ainda tem de renovar
                            $this->updateSubscriptionsInEndOfTerm([$s],null,true);
                        }

                    }
                    //Log::write('info','Atualizados os termos da subscrição #'.$s->id.' com o plano '.$s->plan->name.'.');

                    $result_rows = [
                        'id'=>$s->id
                    ];

                endif;

            endforeach;

      //  endif;

        return $result_rows;
    }


    /**
     * Calcula uma data a ser usada para fim de ciclo
     * através do numero e tipo de intervalo
     *
     * @param datetime $current_term_date Data a ser usada como inicio do calculo
     * @param int $interval_num Quantidade do ciclo (1,2,3...) (plano)
     * @param string $interval_unit Tipo do ciclo (mes,ano etc) (plano)
     * @return date
     */
    public function calculateEndOfTermDate($current_term_date,$interval_num,$interval_unit)
    {
        if(!empty($interval_num) && !empty($interval_unit)){
            $end_date = date("Y-m-d", strtotime("+".$interval_num." ".$interval_unit, strtotime($current_term_date)));
            return $end_date;
        }

        return false;
    }

    /**
     * Método genérico a ser usado para receber dados
     * do Listener do Transactions após um pagamento
     * ser efetuado com sucesso.
     * Com esta info podemos mudar o estado do foreign_key que o Invoice invoca.
     * 
     * Recebe o objecto completo do Invoice.
     *
     * Alterar as datas do termos caso a Subscriptions seja dada como paga
     * 
     * @param object $invoice
     * @return void
     */
    public function transactionPaidWithSuccess($invoice)
    {
        //debug($invoice);
    }

    /**
     * Criar subscrição para um membro
     *
     * @param Array $subscription Tem de conter o plan_id mais as configurações pretendidas para a Subscrição
     * @param Bool $invoice True / False consoante o que se precisar
     * @return void
     */
    public function createSubscription($subscription,$invoice=false)
    {

        $subsObj = $this->newEntity();

        //Get product info
        $plan = $this->Plans->get($subscription['plan_id']);
        if(empty($subscription['current_term_ends_at'])){
            $subscription['current_term_ends_at'] = $this->calculateEndOfTermDate($subscription['current_term_starts_at'], $plan->interval_num, $plan->interval_unit);
        }
        $subscription['tax_percentage'] = $plan->tax_percentage;

        $subsObj = $this->patchEntity($subsObj, $subscription);
        
        if ($this->save($subsObj)) {
            if($invoice){
                //Gera o invoice
                $this->generateInvoiceAfterCreate($subscription);
            }

            return $subsObj;
        }

        return;
        
    }


    /**
     * Undocumented function
     *
     * @param String $debt
     * @param String $quote_amount Valor da subscrição
     * @return void
     */
    public function calculateCurrentTermDateByDebtAmount($debt,$quote_month_value,$debt_date)
    {
        //Calcula o numero de meses que está em divida
        $months_in_debt = $debt/$quote_month_value;
        $last_date = date("Y-m-d", strtotime("-".$months_in_debt." Months", strtotime($debt_date)));

        return $last_date;
    }


}
