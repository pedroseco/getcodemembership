<?php
namespace GetcodeMembership\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Log\Log;
use Cake\Log\LogTrait;
/**
 * Members Model
 *
 * @method \GetcodeMembership\Model\Entity\Member get($primaryKey, $options = [])
 * @method \GetcodeMembership\Model\Entity\Member newEntity($data = null, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Member[] newEntities(array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Member|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \GetcodeMembership\Model\Entity\Member patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Member[] patchEntities($entities, array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Member findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MembersTable extends Table
{

    use LogTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('gc_members');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Subscriptions', [
            'foreignKey' => 'member_id',
            'className' => 'GetcodeMembership.Subscriptions'
            ]
        );

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'GetcodeMembership.Users'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('num')
            ->allowEmpty('num');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name', 'O campo Nome é de preenchimento obrigatório!');

        $validator
            ->integer('idnumber')
            ->allowEmpty('idnumber');

        $validator
            ->date('birthdate')
            ->allowEmpty('birthdate');

        $validator
            ->allowEmpty('local');

        $validator
            ->allowEmpty('address');

        $validator
            ->integer('phone')
            ->allowEmpty('phone');

        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email','O campo Email é de preenchimento obrigatório!');

        $validator
            ->date('admission_date')
            ->allowEmpty('admission_date');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        $validator
            ->date('last_payment_date')
            ->allowEmpty('last_payment_date');

        $validator
            ->decimal('last_payment_value')
            ->allowEmpty('last_payment_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    public function beforeSave($event, $entity, $options) {
        
        if(empty($entity->id)){
            //Só manipulamos os dados SE foi um Insert e não um Update
            $user = $this->validateAndCreateUserIfDoesntExists($entity);
            if(!empty($user)){
                $entity->user_id = $user->id;
            }
        }
        
    }
    /**
     * Devolve Invoices associados procurando
     * pelos associados a uma subscrição caso
     * existam.
     * 
     * @return void
     * @return array with invoice id's
     */
    public function getInvoiceArrayFromMemberSubscriptions($member)
    {
        $i=[];

        if(!empty($member->subscriptions)){
            foreach($member->subscriptions as $subscription){
                if(!empty($subscription->invoices)){

                    foreach($subscription->invoices as $invoice){
                        if(!array_key_exists($invoice->id,$i)){
                            $i[]=$invoice->id;
                        }
                    }
                }
            }
        }

        if(!empty($i)){
            $tableInvoices=TableRegistry::get('GetcodePayments.Invoices');
            $invoices_array=$tableInvoices->find()->where(['id IN'=>$i])->toArray();
            return $invoices_array;
        }

        return null;
        
    }

    /**
     * Devolve o total em dívida do membro,
     * calculando o total de cada invoice onde o 
     * estado seja "em divida" ou 0
     *
     * @param array $invoices array from getInvoiceArrayFromMemberSubscriptions()
     * @return void
     */
    public function getDueInvoicesTotal($invoices)
    {
        $total=0;
        if(!empty($invoices)){
            foreach($invoices as $i){
                //detecta apenas os que estao por pagar
                if($i['status'] == 0 || is_null($i['status'])){
                    //usa o balance pq o invoice pode descontar valores
                    $total += $i['balance']; 
                }
            }
        }
        return $total;
    }

    /**
     * Devolve o total pago do membro,
     * calculando o total de cada invoice onde o 
     * estado seja "pago" ou 1
     *
     * @param array $invoices array from getInvoiceArrayFromMemberSubscriptions()
     * @return void
     */
    public function getPaidInvoicesTotal($invoices)
    {
        $total=0;
        if(!empty($invoices)){
            foreach($invoices as $i){
                //detecta apenas os que estao por pagar
                if($i['status'] == 1){
                    //usa o balance pq o invoice pode descontar valores
                    $total += $i['balance']; 
                }
            }
        }
        return $total;
    }



    /**
     * Processamento do CSV no Import
     */
    public function processCsvImport($path)
    {
        //Inicio do CSV
        $csv = Reader::createFromPath($path, 'r');
        $csv->setHeaderOffset(0);
        $header = $csv->getHeader();
        $records = $csv->getRecords();        
        $_records = (new Statement())->process($csv);
        
        $members = [];

        $entity_table = array_flip(Configure::read('csv_table_headers'));
         
        $i=0;
        foreach ($_records as $record) {
            if(array_key_exists('ID',$record)){
                unset($record['ID']);
            }
            foreach($record as $k=>$v){
                if(!empty($entity_table[$k])){
                    $members[$i][$entity_table[$k]] = $v;
                }
            }
            $i++;
        }

        return $members;
        
    }

    /**
     * Processa a exportação de membros
     *
     * @param Array $data resultado de um find()->toArray()
     * @param string $path caminho do ficheiro
     * @return void
     */
    public function processCsvExport(Array $data,$path,$filename)
    {
        $writer = Writer::createFromPath($path,'w+');
        $writer->insertAll($data);
        $writer->output($filename);
        die;
    }


    /** 
     * Exemplo de CSV
     */
    public function csvexample()
    {

        $records = [
            ['1081','1005','Joana Gouveia','6922728','18-06-1955','Lourinhã','91766666','emaildapessoa@gmail.com','Professora','2017-09-20','1','Anual'],
            ['1082','1006','Zé','1922728','18-06-1995','Varatojo','261999222','emaildapessoa@gmail.com','Futebolista','2016-09-20','1','Anual'],
        ];

        $uploaddir = WWW_ROOT.'files/csv_exemplo.csv';

        $writer = Writer::createFromPath($uploaddir,'w+');
        $writer->insertOne(['ID','NºSÓCIO','NOME','NºBI','DATA NASC.','LOCALIDADE','TEL.','EMAIL','PROFISSÃO','DATA','ADMISSAO','ESTADO','QUOTIZACAO']);
        $writer->insertAll($records); //using an array
        $writer->output('exemplo_csv.csv');
        die;        
    }


    /** USER RELATED **/


    /**
     * Cria um Membro com os parametros mínimos
     *
     * @param [type] $data Object
     * @return void
     */
    public function createMember($data)
    {

      
        $member = $this->newEntity();
        $member = $this->patchEntity($member, $data);

 
        if ($this->save($member)) {

            //user ja existe? validar email
            $find_user=$this->validateAndCreateUserIfDoesntExists($member);
            if(!$find_user){
                return 'Membro criado, mas User não foi criado';
            }

            return $member;
        }else{
            debug($member);
        }
        
        return;
    }

    /**
     * Valida se um user existe após crair um mebro
     * e cria o utilizador devolvendo o record.
     *
     * @param [type] $email
     * @return void
     */
    public function validateAndCreateUserIfDoesntExists($data)
    {
        $userExists = $this->findIfUserExistsByEmail($data->email);
        if(!$userExists){
            //dados para novo record
            $request_data = [
                'first_name'=>$data->name,
                'email'=>$data->email,
                'password'=>$data->password,
                'role'=>'user'
            ];            
            //criação de user
            $tableUsrs = TableRegistry::get('GetcodeCms.Usrs');
            $new_user = $tableUsrs->_createUser($request_data);

            return $new_user;
        }

        return $userExists;
    }
    
    /** 
     * Procura por um utilizador por email
     * 
     * @param string $email
     */
    public function findIfUserExistsByEmail($email)
    {
      
      $usersTable = TableRegistry::get('Users');
      $user=$usersTable->find()
        ->where(['email'=>$email])
        ->first();

      return $user;
    }


    /**
     * Processo de aprovação de membro
     * Necessita de uma auteticação e respetivas permissões para esta aprovação
     * @return object registo do membro aprovado
     */
    public function approveMembershipProcess($member_id)
    {
        
        $approved_member = $this->approveMember($member_id);
        if(!empty($approved_member)){
            
            $event = new Event('Model.Members.approve_member', $this, [
                'member'=>$approved_member
                ]);
            $this->eventManager()->dispatch($event);

            $this->log('Membro aprovado: '.$member_id.' em '.date('Y-m-d'), 'debug');

            return $approved_member;
        }
        
        return false;
    }

    /**
     * Muda o estado do Membro validando
     * a operação
     *
     * @param string Id do membro
     * @return void
     */
    private function approveMember($member_id)
    {
        $m=$this->get($member_id);
        //apenas aprova membros onde estado = 0
        if(!empty($m) && $m->status == 0){
            $m->status = 1;
            //caso o membro ainda nao tenha um numero atribuido
            //se nao for null é porque já existe um num associado anteriormente
            if($m->num == null){
                $m->num = $this->generateMemberNumber();
            }
            if($this->save($m)){
                return $m;
            }        
        }
        return false;
    }

    /**
     * Gera novo numero de sócio
     *
     * @return void
     */
    public function generateMemberNumber(){
        
        //gets the last aproved member
        $last_approved_member = $this->find()
            ->where(['Members.status'=>1])
            ->order(['Members.num'=>'DESC'])
            ->select(['Members.num'])
            ->first();
        //adds +1 to the member number
        $member_num = $last_approved_member->num + 1;
        
        return $member_num;
    }

    /** 
     * Método para alterar numero do membro
     * caso o numero alterado já esteja atribuido
     * a outro membro
     * 
     * $member só é populado caso seja no edit
     * 
     * @param $member null - Object membro
     * @param $number int numero
     */
    public function changeMemberNumber($member = null,$number){
        
        $existing_member = $this->find()
            ->where(['Members.num'=>$number])
            ->select(['Members.num'])
            ->first();

        if(!empty($existing_member)){
            $_error = 'Este número já se encontra atribuido a um Membro';
            if($member){
                Log::debug('Membro '.$member->id.' não pode mudar para o numero '.$number.', já existe na Base Dados.');
            }
            return $_error;
        }else{
            return true;
        }

    }

}
