<?php
namespace GetcodeMembership\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Plans Model
 *
 * @property \GetcodeMembership\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 *
 * @method \GetcodeMembership\Model\Entity\Plan get($primaryKey, $options = [])
 * @method \GetcodeMembership\Model\Entity\Plan newEntity($data = null, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Plan[] newEntities(array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Plan|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \GetcodeMembership\Model\Entity\Plan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Plan[] patchEntities($entities, array $data, array $options = [])
 * @method \GetcodeMembership\Model\Entity\Plan findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlansTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('gc_subscriptionplans');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('code', 'create')
            ->notEmpty('code');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->allowEmpty('description');

        $validator
            ->boolean('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('trial_period')
            ->allowEmpty('trial_period');

        $validator
            ->decimal('setup_fee')
            ->allowEmpty('setup_fee');

        $validator
            ->decimal('recurring_price')
            ->allowEmpty('recurring_price');

        $validator
            ->allowEmpty('url');

        return $validator;
    }

}
