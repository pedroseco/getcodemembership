<?php
namespace GetcodeMembership\Event;

use Cake\Log\Log;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Event\EventListenerInterface;
use Cake\Core\Plugin;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

class MembersListener implements EventListenerInterface {


    public function implementedEvents()
    {
        return [
            'Model.Members.import_success' => 'importSuccess',
            'Model.Members.approve_member' => 'approveMember',
            'Model.Members.add_member' => 'addMember',
            'Model.Members.update_member' => 'updateMember',
            'Model.Members.delete_member' => 'deleteMember'
        ];
    }


    /**
     * Membro foi salvo na base de dados com sucesso.
     * Pegar no record e enviar para o Users, criando um novo registo 
     * e alterando o do Membro para conter o id do user_id criado.
     * 
     *
     * @return void
     */
    public function importSuccess(Event $event, $member)
    {   
        if(!Plugin::loaded('GetcodeCms')){
            return false;
        }

        $tableUsrs = TableRegistry::get('GetcodeCms.Usrs');
        $tableMembers = TableRegistry::get('GetcodeMembership.Members');
        $user = $tableUsrs->_createUser($member);

        if(!empty($user->id)){
            //User criado com sucesso
            $member->user_id = $user->id;
            $tableMembers->save($member);
        }
        
        return true;
    }


    /**
     * Quando um membro é aprovado através do método approveMember
     * este listener é chamado
     *
     * @param Event $event
     * @param object $member
     * @return void
     */
    public function approveMember(Event $event, $member)
    {
        Log::write('info','Membro aprovado: #'.$member->id.' - '.$member->name.'  em '.date('Y-m-d'));
        return true;
    }



    /**
     * Quando um membro é adicionado
     *
     * @param Event $event
     * @param object $member
     * @return void
     */
    public function addMember(Event $event, $member)
    {
        Log::write('info','Novo membro adicionado: '.$member->name);
        return true;
    }


    /**
     * Quando é atualizado os dados de um membro
     *
     * @param Event $event
     * @param object $member
     * @return void
     */
    public function updateMember(Event $event, $member)
    {
        Log::write('info','Dados do membro '.$member->name.' atualizado.');
        return true;
    }


    /**
     * Quando um membro é removido
     *
     * @param Event $event
     * @param object $member
     * @return void
     */
    public function deleteMember(Event $event, $member)
    {
        Log::write('info','Registo  relativo ao membro'.$member->name.' removido.');
        return true;
    }

}
