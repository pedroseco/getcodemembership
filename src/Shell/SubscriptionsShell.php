<?php
namespace GetcodeMembership\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;
use Cake\Log\Log;
use Cake\Utility\Text;
use League\Csv\Reader;
use League\Csv\Statement;
use League\Csv\Writer;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\ORM\TableRegistry;

class SubscriptionsShell extends Shell
{

    public $modelClass = 'GetcodeMembership.Subscriptions';


    public function main()
    {
        $this->out('Consola Shell das subcrições');
    }

    /**
     * Valida as subscrições disponíveis e ativas
     * procurando pelas datas de current_term_ends_at para 
     * gerar novas datas, criar invoices e alterar os estados respetivos
     *
     * @return void
     */
    public function findEndOfTermSubscriptionsAndRenew()
    {
        $renewal_days_before= empty(Configure::read('Settings.Subscriptions.renewal_days_before'))?0:Configure::read('Settings.Subscriptions.renewal_days_before');
        $current_date = date('Y-m-d', strtotime('+ '.$renewal_days_before.' days'));

        $current_date = date('Y-m-d', strtotime('2019-01-01'));

        //Procura por subscrições quem chegam ao fim á data de hoje
        $subs = $this->Subscriptions->findSubscriptionsInEndOfTerm($current_date);
        
        //Edita e corre as ações necessárias a estas subscrições
        $updated_result = $this->Subscriptions->updateSubscriptionsInEndOfTerm($subs,$current_date,true);
    
        $this->out('Update(s) efetuado(s) com sucesso: '.count($updated_result).' subscrições.');

    }





    public function importMembers()
    {

        $this->out('ImportarCsv');
       //Inicio do CSV
        $csv = Reader::createFromPath(WWW_ROOT.DS.'primeiro.csv', 'r');
        $csv->setHeaderOffset(0);
        $header = $csv->getHeader();
        $records = $csv->getRecords();        
        $_records = (new Statement())->process($csv);
        
        $members = [];


        $members_table = TableRegistry::get('GetcodeMembership.Members');
        $subscriptions_table = TableRegistry::get('GetcodeMembership.Subscriptions');
        
         
        $i=0;
        foreach ($_records as $record) {

            $record['num'] = trim($record['num']);
            $record['phone'] = preg_replace('/\s+/', '', $record['phone']);

            if(!is_numeric($record['idnumber'])){
                $record['idnumber'] = 0;
            }

            if(!is_numeric($record['phone'])){
                $record['phone'] = 0;
            }

            //Criar Membro
            if($record['status'] == 'Activo'){
                $record['status'] = 1;
            }else{
                $record['status'] = 0;
            }

            if(empty($record['quote_debt'])){
                $record['quote_debt'] = 0;
            }

            if(empty($record['email']) || empty(strpos($record['email'], '@'))){
                $record['email'] = 'noreply_'.round(microtime(true) * 1000).'@getcode.pt';
            }

            if(!empty($record['birthdate'])){
                $record['birthdate'] = date('Y-m-d',strtotime($record['birthdate']));
            }else{
                $record['birthdate'] = date('Y-m-d',strtotime('01-01-1980'));
            }

            //Tipos de Quota (neste caso sao so 2 Semestral e Anual)
            if($record['quote_duration'] == 'Anual'){

                $record['plan_id'] = 5;
                $record['quote_amount'] = 24;

            }elseif($record['quote_duration'] == 'Semestral'){

                $record['plan_id'] = 4;
                $record['quote_amount'] = 12;

            }else{
                $record['quote_duration'] = 'Anual';
                $record['plan_id'] = 4;
                $record['quote_amount'] = 0;
            }

       
            $member = $members_table->createMember($record);

            if(!empty($member->id)){
                
                $last_date=$subscriptions_table->calculateCurrentTermDateByDebtAmount($record['quote_debt'],2,'2019-01-01');

                $subs = [
                    'status'=> 1,
                    'amount'=> $record['quote_amount'],
                    'current_term_starts_at'=>$last_date,
                    'last_billing_at' =>'',
                    'next_billing_at' =>'',
                    'plan_id' => $record['plan_id'],
                    'member_id'=> $member->id,
                    'expires_at' => null,
                    'activated_at' => date('Y-m-d', strtotime('NOW'))
                ];

                $result = $subscriptions_table->createSubscription($subs,false);
            }

        }

  
        return $members;

    }
}