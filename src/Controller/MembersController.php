<?php
namespace GetcodeMembership\Controller;

use GetcodeMembership\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Utility\Hash;
/**
 * Members Controller
 *
 *
 * @method \GetcodeMembership\Model\Entity\Member[] paginate($object = null, array $settings = [])
 */
class MembersController extends AppController
{



    public function initialize()
    {
      parent::initialize();
      $this->Auth->allow(['add']);
      
    }


    /**
     * View method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $member = $this->Members->get($id, [
            'contain' => ['Subscriptions','Subscriptions.Invoiceitems','Subscriptions.Invoices']
        ]);
        
        //Devolve array com ID's dos invoices associados a este membro
        $invoices = $this->Members->getInvoiceArrayFromMemberSubscriptions($member);
        
        //Total em divida
        $due_total= $this->Members->getDueInvoicesTotal($invoices);
        
        //Total pago
        $paid_total= $this->Members->getPaidInvoicesTotal($invoices);
        

        $this->set(compact('member','invoices','due_total','paid_total'));
        $this->set('_serialize', ['member','invoices','due_total','paid_total']);
    }


    /**
     * Método para adicionar Membro
     * 
     * Em casos especificos, o "data" retorna dados adicionais aos definidos
     * na base de dados. Neste caso os dados são gravados no campo "extra_fields"
     *
     * @return \Cake\Http\Response|null Rediciona para o login após sucesso
     */
    public function add()
    {
        
        $member = $this->Members->newEntity($this->request->getData());
        if ($this->request->is('post')) {

            //verificar se os extra_fields estao ser devolvidos como array()
            //caso este cenario seja verdade, guardar todos os campos como json
            if(isset($this->request->data['extra_fields']) && is_array($this->request->data['extra_fields'])){
                $this->request->data['extra_fields'] = json_encode($this->request->data['extra_fields'], true);
            }
        
            $member = $this->Members->patchEntity($member, $this->request->getData());

            if ($this->Members->save($member)) {
                //TODO: gerar um invoice com pagamento da cota (anual/semetral/mensal)
                $this->Flash->success(__('Registo efectuado com sucesso!'));
                return $this->redirect(['controller'=>'Usrs','action'=>'login','plugin'=>'GetcodeCms']);
            }else{
                if($member->errors()){
                  $error_msg = [];
                  foreach( $member->errors() as $errors){
                      if(is_array($errors)){
                          foreach($errors as $error){
                              $error_msg[]    =   $error;
                          }
                      }else{
                          $error_msg[]    =   $errors;
                      }
                  }
                  if(!empty($error_msg)){
                      $this->Flash->error(
                          __("Corriga os seguinte(s) erro(s): <p><strong>".implode("\n \r", $error_msg)."</strong></p>"),['escape' => false]
                      );
                      return $this->redirect($this->request->referer());
                  }
                }
            }
        }

        //List all subscriptions
        $plans = $this->Members->Subscriptions->Plans->find('list',['keyField'=>'id','valueField'=>'name']);

        $this->set(compact('member','plans'));
        $this->set('_serialize', ['member','plans']);
    }


    /**
     * Edit method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $member = $this->Members->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $member = $this->Members->patchEntity($member, $this->request->getData());
            if ($this->Members->save($member)) {
                $this->Flash->success(__('Registo editado com sucesso.'));
                return $this->redirect($this->request->referer());
            }
            $this->Flash->error(__('Ocorreu um erro ao tentar editar o registo. Verifique se preencheu todos os campos.'));
        }
        $this->set(compact('member'));
        $this->set('_serialize', ['member']);
    }


    /**
     * Delete method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        if(empty($id) && !empty($this->request->data['ids'])){
          //bulk delete
          foreach($this->request->data['ids'] as $key=>$val){
            if (!empty($val)) {
              $members = $this->Members->get($val);
              $this->Members->deleteAll(['id' => $val]);
            }
          }
          $this->Flash->success(__('Registos removidos com sucesso'));

        }else{

          $members = $this->Members->get($id);
          if ($this->Members->delete($members)) {
              $this->Flash->success(__('Registo removido com sucesso.'));
          } else {
              $this->Flash->error(__('Erro ao remover o registo.'));
          }

        }

        return $this->redirect(['action' => 'index']);
    }



}
