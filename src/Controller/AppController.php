<?php

namespace GetcodeMembership\Controller;

use App\Controller\AppController as BaseController;

class AppController extends BaseController
{
    public function initialize()
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('GetcodeCms.default');
  
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
        $this->loadComponent('CakeDC/Users.UsersAuth');

    }


}
