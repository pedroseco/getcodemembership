<?php
namespace GetcodeMembership\Controller\Admin;

use GetcodeMembership\Controller\AppController;

use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\NotFoundException;

/**
 * Subscriptions Controller
 *
 * @property \GetcodeMembership\Model\Table\SubscriptionsTable $Subscriptions
 *
 * @method \GetcodeMembership\Model\Entity\Subscription[] paginate($object = null, array $settings = [])
 */
class SubscriptionsController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('GetcodeSettings.ExportCsv');
        $this->loadComponent('BryanCrowe/ApiPagination.ApiPagination');

        /**
         * Hack para retorar o string que é escapado
         * pelo vue-table ao pedir o pagination
         */
        if(isset($this->request->query['sort'])){
            $res=explode('#',$this->request->query['sort']);
            if(!empty($res[1])){
                $this->request->query['sort'] = $res[0];
                $this->request->query['direction']= $res[1];
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'page'=>1,
            'limit'=>15,
            'maxLimit' => 100,
            'contain' => ['Plans', 'Members']
        ];
        
        if(isset($this->request->query['filter'])){
            $this->paginate['conditions'] = [
                'Subscriptions.name LIKE' => '%'.$this->request->query['filter'].'%'
            ];
        }
        
        $query = $this->Subscriptions->find()
            ->where(['NOT'=>[
                'Subscriptions.status' => 3
            ]]);

        $data = $this->paginate($query);

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);
    }

        /**
     * Método para exportação dos dados
     * Procura por todos os resultados na bd
     * e devolve os registos correspondentes
     *
     * @param string passar a extenção no url (.csv ou .json)
     * @return void json or csv
     */
    public function export()
    {
        $subscriptions = $this->Subscriptions->find();

        $this->set(compact('subscriptions'));
        $this->set('_serialize',['subscriptions']);
        $doExport=$this->ExportCsv->doExport();
        if($doExport){
            $this->viewBuilder()->className('CsvView.Csv');
            $this->set($doExport);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Subscription id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subscription = $this->Subscriptions->get($id, [
            'contain' => ['Plans', 'Members']
        ]);

        $this->set('subscription', $subscription);
        $this->set('_serialize', ['subscription']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $subscription = $this->Subscriptions->newEntity();
        if ($this->request->is('post')) {
            

            //subscription first curret_term_date is the same as the activated_at date, for the first time
            $this->request->data['current_term_starts_at'] = new Time($this->request->getData('activated_at'));       
            
            //calculate end of term date
            //Get product info
            $plan = $this->Subscriptions->Plans->get($this->request->getData('plan_id'));
            $this->request->data['current_term_ends_at'] = $this->Subscriptions->calculateEndOfTermDate($this->request->data['current_term_starts_at'],$plan->interval_num,$plan->interval_unit);
            
            //Tax percentage from Plan
            $this->request->data['tax_percentage'] = $plan->tax_percentage;
            
            $subscription = $this->Subscriptions->patchEntity($subscription, $this->request->getData());

            if ($this->Subscriptions->save($subscription)) {

                //Gera o invoice
                $this->Subscriptions->generateInvoiceAfterCreate($subscription);

                Log::write('info','Invoice gerado através de subscrição: #'.$subscription->id.'.');

                $this->Flash->success(__('Subscrição criada com sucesso'));
                return $this->redirect(['action' => 'index']);

            }else{

                if($subscription->errors()){
                    $error_msg = [];
                    foreach( $subscription->errors() as $errors){
                     
                        if(is_array($errors)){
                            foreach($errors as $k=>$v){
                                 $error_msg[]    =   key($subscription->errors()).' - '.$v;
                            }
                        }else{
                            $error_msg[]    =   $errors;
                        }
                    }
                    if(!empty($error_msg)){
                      
                        $this->Flash->error(
                            __("Corriga os seguinte(s) erro(s): <p><strong>".implode("\n \r", $error_msg)."</strong></p>"),['escape' => false]
                        );
                        return $this->redirect($this->request->referer());
                    }
                }
                
            }
            
        }
        
        $plans = $this->Subscriptions->Plans->find('list', ['limit' => 200]);
        $members = $this->Subscriptions->Members->find('list', ['limit' => 200]);
        $methods = TableRegistry::get('GetcodePayments.Methods')->find('list',['keyField'=>'id','ValueField'=>'title']);

        $this->set(compact('subscription', 'plans', 'members','methods'));
        $this->set('_serialize', ['subscription']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Subscription id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $subscription = $this->Subscriptions->get($id);

        /** Para quando o registo está como "soft_delete"
         * status pré-definido no ficheiro bootstrap do plugin
         */
        if($subscription->status == 3){
            throw new NotFoundException("Não tem acesso ao conteúdo escolhido.", 1);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $date = new Time($this->request->getData('current_term_starts_at'));
   
            $subscription = $this->Subscriptions->patchEntity($subscription, $this->request->getData());
            if ($this->Subscriptions->save($subscription)) {
                $this->Flash->success(__('The subscription has been saved.'));
                return $this->redirect(['action' => 'index']);
            }else{
                if($subscription->errors()){
                  $error_msg = [];
                  foreach( $subscription->errors() as $errors){
                      if(is_array($errors)){
                          foreach($errors as $error){
                              $error_msg[]    =   $error;
                          }
                      }else{
                          $error_msg[]    =   $errors;
                      }
                  }
                  if(!empty($error_msg)){
                      $this->Flash->error(
                          __("Corriga os seguinte(s) erro(s): <p><strong>".implode("\n \r", $error_msg)."</strong></p>"),['escape' => false]
                      );
                      return $this->redirect($this->request->referer());
                  }
                }
            }
        }
        $plans = $this->Subscriptions->Plans->find('list', ['limit' => 200]);
        $members = $this->Subscriptions->Members->find('list', ['limit' => 200]);
        $methods = TableRegistry::get('GetcodePayments.Methods')->find('list',['keyField'=>'id','ValueField'=>'title']);

        $this->set(compact('subscription', 'plans', 'members','methods'));
        $this->set(compact('subscription', 'plans', 'members'));
        $this->set('_serialize', ['subscription']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Subscription id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        if(empty($id) && !empty($this->request->data['ids'])){
            //bulk delete
            foreach($this->request->data['ids'] as $key=>$val){
              if (!empty($val)) {
                $subscription = $this->Subscriptions->get($val);
                //$this->Subscriptions->deleteAll(['id' => $val]);
                $subscription->status = 3;
                $this->Subscriptions->save($subscription);
              }
            }
            $this->Flash->success(__('Dado(s) eliminado(s) com sucesso.'));
  
        }else{

            $subscription = $this->Subscriptions->get($id);
            $subscription->status = 3;
            if ($this->Subscriptions->save($subscription)) {
                $this->Flash->success(__('Dado(s) eliminado(s) com sucesso.'));
            } else {
                $this->Flash->error(__('Erro ao eliminar dado(s).'));
            }

        }

        return $this->redirect(['action' => 'index']);
    }
}
