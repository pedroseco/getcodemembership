<?php
namespace GetcodeMembership\Controller\Admin;

use GetcodeMembership\Controller\AppController;

/**
 * Plans Controller
 *
 * @property \GetcodeMembership\Model\Table\PlansTable $Plans
 *
 * @method \GetcodeMembership\Model\Entity\Plan[] paginate($object = null, array $settings = [])
 */
class PlansController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('GetcodeSettings.ExportCsv');
        $this->loadComponent('BryanCrowe/ApiPagination.ApiPagination');

        /**
         * Hack para retorar o string que é escapado
         * pelo vue-table ao pedir o pagination
         */
        if(isset($this->request->query['sort'])){
            $res=explode('#',$this->request->query['sort']);
            if(!empty($res[1])){
                $this->request->query['sort'] = $res[0];
                $this->request->query['direction']= $res[1];
            }
        }
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $this->paginate = [
            'page'=>1,
            'limit'=>15,
            'maxLimit' => 100
        ];
        
        if(isset($this->request->query['filter'])){
            $this->paginate['conditions'] = [
                'Plans.name LIKE' => '%'.$this->request->query['filter'].'%'
            ];
        }

        $data = $this->paginate($this->Plans);

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);

    }

    /**
     * View method
     *
     * @param string|null $id Plan id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $plan = $this->Plans->get($id);

        $this->set('plan', $plan);
        $this->set('_serialize', ['plan']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $plan = $this->Plans->newEntity();
        if ($this->request->is('post')) {

            $plan = $this->Plans->patchEntity($plan, $this->request->getData());
            if ($this->Plans->save($plan)) {
                $this->Flash->success(__('The plan has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Ocorreu um erro ao tentar adicionar o registo. Verifique se preencheu todos os campos.'));
        }

        $this->set(compact('plan'));
        $this->set('_serialize', ['plan']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Plan id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $plan = $this->Plans->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $plan = $this->Plans->patchEntity($plan, $this->request->getData());
            if ($this->Plans->save($plan)) {
                $this->Flash->success(__('The plan has been saved.'));

                return $this->redirect(['action' => 'index']);
            }else{
                if($plan->errors()){
                  $error_msg = [];
                  foreach( $plan->errors() as $errors){
                      if(is_array($errors)){
                          foreach($errors as $error){
                              $error_msg[]    =   $error;
                          }
                      }else{
                          $error_msg[]    =   $errors;
                      }
                  }
                  if(!empty($error_msg)){
                      $this->Flash->error(
                          __("Corriga os seguinte(s) erro(s): <p><strong>".implode("\n \r", $error_msg)."</strong></p>"),['escape' => false]
                      );
                      return $this->redirect($this->request->referer());
                  }
                }
            }
        }

        $this->set(compact('plan'));
        $this->set('_serialize', ['plan']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Plan id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $plan = $this->Plans->get($id);
        if ($this->Plans->delete($plan)) {
            $this->Flash->success(__('The plan has been deleted.'));
        } else {
            $this->Flash->error(__('Ocorreu um erro ao tentar remover o registo.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
