<?php
namespace GetcodeMembership\Controller\Admin;

use GetcodeMembership\Controller\AppController;
use Cake\Network\Exception\ForbiddenException;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Utility\Hash;
/**
 * Members Controller
 *
 *
 * @method \GetcodeMembership\Model\Entity\Member[] paginate($object = null, array $settings = [])
 */
class MembersController extends AppController
{

    public function initialize()
    {
      parent::initialize();  
      $this->loadComponent('RequestHandler');
      $this->loadComponent('GetcodeSettings.ExportCsv');
      $this->loadComponent('BryanCrowe/ApiPagination.ApiPagination');

      /**
       * Hack para retorar o string que é escapado
       * pelo vue-table ao pedir o pagination
       */
      if(isset($this->request->query['sort'])){
          $res=explode('#',$this->request->query['sort']);
          if(!empty($res[1])){
              $this->request->query['sort'] = $res[0];
              $this->request->query['direction']= $res[1];
          }
      }
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'page'=>1,
            'limit'=>25,
            'maxLimit' => 100,
            'contain' => ['Subscriptions']
        ];
        
        if(isset($this->request->query['filter'])){
            $this->paginate['conditions'] = [
                'OR'=>[
                    'Members.name LIKE' => '%'.$this->request->query['filter'].'%',
                    'Members.email LIKE' => '%'.$this->request->query['filter'].'%'
                ]
            ];
        }

        $data = $this->paginate($this->Members);

        $this->set(compact('data'));
        $this->set('_serialize', ['data']);

    }


    /**
     * View method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $member = $this->Members->get($id, [
            'contain' => ['Subscriptions','Subscriptions.Invoiceitems','Subscriptions.Invoices']
        ]);
        
        //Devolve array com ID's dos invoices associados a este membro
        $invoices = $this->Members->getInvoiceArrayFromMemberSubscriptions($member);
        
        //Total em divida
        $due_total= $this->Members->getDueInvoicesTotal($invoices);
        //Total pago
        $paid_total= $this->Members->getPaidInvoicesTotal($invoices);
        

        $this->set(compact('member','invoices','due_total','paid_total'));
        $this->set('_serialize', ['member','invoices','due_total','paid_total']);
    }

    /**
     * Adicionar um membro
     * @return void
     */
    public function add()
    {

        $member = $this->Members->newEntity();
        
        //caso tenha user_id associado do ecra de Utilizador
        if(!empty($this->request->query)){
            $member->email = !empty($this->request->query['user_email'])?$this->request->query['user_email']:null;
            $member->name = !empty($this->request->query['user_firstname'])?$this->request->query['user_firstname']:null;
        }

        if ($this->request->is('post')) {


            //Quando o número do membro é alterado
            if(!empty($this->request->data['num'])){
                $changemember = $this->Members->changeMemberNumber(null,$this->request->data['num']);
                if(!empty($changemember)){                    
                    $this->Flash->error($changemember);
                    return $this->setAction(['action'=>'add']);
                }   
            }

            /**
             * Gerar um numero quando o Membro
             * é dado como ativo no ato do registo
             **/
            if($this->request->data['status'] == 1){
                $this->request->data['num'] = $this->Members->generateMemberNumber();
            }
                    
            $member = $this->Members->patchEntity($member, $this->request->getData());

            //user ja existe?
            //validar email
            $find_user=$this->Members->findIfUserExistsByEmail($member->email);
            //existe, e envia notificação para o site.

            if ($this->Members->save($member)) {

                if(!empty($find_user->id)){
                    $this->Flash->success(__('O email selecionado já pertence a um utilizador. Se não pretende associar o registo, edite a ficha de utilizador.'));
                }

                $this->Flash->success(__('Membro adicionado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Ocorreu um erro ao tentar adicionar o registo. Verifique se preencheu todos os campos.'));
        }

        $users = TableRegistry::get('Users')->find('list',['keyField'=>'id','valueField'=>'username'])
            ->where(['Users.role'=>'user']);

        $last_num = $this->Members->find()
            ->order(['num' => 'DESC'])
            ->select(['num'])
            ->first();

        $last_member_number = $last_num->num;
        
        $this->set(compact('member','users','last_member_number'));
        $this->set('_serialize', ['member','users','last_member_number']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $member = $this->Members->get($id);
 
        $member->birthdate = !empty($member->birthdate)?$member->birthdate->i18nFormat('YYY-MM-dd'):null;
        $member->admission_date = !empty($member->admission_date)?$member->admission_date->i18nFormat('YYY-MM-dd'):null;

        if ($this->request->is(['patch', 'post', 'put'])) {
            
            //Quando o número do membro é alterado
            if(!empty($this->request->data['num']) && $this->request->data['num'] != $member->num){
                $changenumber = $this->Members->changeMemberNumber($member,$this->request->data['num']);
                
                if($changenumber !== true){                    
                    $this->Flash->error($changenumber);
                    return $this->redirect(['action'=>'edit', $member->id]);
                }
            }

            $member = $this->Members->patchEntity($member, $this->request->getData());
            if ($this->Members->save($member)) {
                $this->Flash->success(__('Registo editado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }else{
                if($member->errors()){
                  $error_msg = [];
                  foreach( $member->errors() as $k => $errors){
                      if(is_array($errors)){
                          foreach($errors as $error){
                              $error_msg[]    =   $k .' - '.$error;
                          }
                      }else{
                          $error_msg[]    =   $k .' - '.$errors;
                      }
                  }

                  if(!empty($error_msg)){
                      $this->Flash->error(
                          __("Corriga os seguinte(s) erro(s): <p><strong>".implode("\n \r", $error_msg)."</strong></p>"),['escape' => false]
                      );
                      return $this->redirect($this->request->referer());
                  }
                }
            }
        }

        $users = TableRegistry::get('Users')->find('list',['keyField'=>'id','valueField'=>'username'])
            ->where(['Users.role'=>'user']);

        $last_num = $this->Members->find()
            ->order(['num' => 'DESC'])
            ->select(['num'])
            ->first();

        $last_member_number = $last_num->num;
        
        $this->set(compact('member','users','last_member_number'));
        $this->set('_serialize', ['member','users','last_member_number']);
        
    }

    /**
     * Delete method
     *
     * @param string|null $id Member id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        if(empty($id) && !empty($this->request->data['ids'])){
          //bulk delete
          foreach($this->request->data['ids'] as $key=>$val){
            if (!empty($val)) {
              $members = $this->Members->get($val);
              $this->Members->deleteAll(['id' => $val]);
            }
          }
          $this->Flash->success(__('Registos removidos com sucesso'));

        }else{

          $members = $this->Members->get($id);
          if ($this->Members->delete($members)) {
              $this->Flash->success(__('Registo removido com sucesso.'));
          } else {
              $this->Flash->error(__('Erro ao remover o registo.'));
          }

        }

        return $this->redirect(['action' => 'index']);
    }


    /** 
     * Importação de dados de membros
     * através de CSV
     */
    public function import()
    {
        $formdata=$this->Members->newEntity();
        
        if(!empty($this->request->data)){

            if($this->request->data['file']['type'] != 'text/csv'){
                $this->Flash->error(__('Apenas são permitidos ficheiros do tipo .CSV'));
                return;
            }
   
            //Local temporario
            $uploaddir = WWW_ROOT.'/files/csv/';
            if (!file_exists($uploaddir)) {
                mkdir($uploaddir, 0777, true);
            }
            $uploadfile = $uploaddir . basename($this->request->data['file']['name']);


            if (move_uploaded_file($this->request->data['file']['tmp_name'], $uploadfile)){

                $members=$this->Members->processCsvImport($uploadfile);
        
                if($members){
                    $this->set(compact('members','uploadfile'));
                    $this->render('/admin/Members/confirmimport');            
                }

            }else{
                $this->Flash->error(__('Não foi possível salvar o ficheiro no sistema. Verifique se o mesmo não está corrupto, ou excede a dimensão máxima permitida.'));
            }

        }

        $this->set(compact('formdata'));
        $this->set('_serialize', ['formdata']);
    }


    /**
     * Exportação completa de dados
     * 
     * @return void
     */
    public function export()
    {
        
        $members = $this->Members->find();

        $this->set(compact('members'));
        $this->set('_serialize',['members']);


        $doExport=$this->ExportCsv->doExport();
        $_delimiter = ';';
        if($doExport){
            $this->viewBuilder()->className('CsvView.Csv');
            $this->set(compact('_delimiter'));
            $this->set($doExport);
        }


    }


    /**
     * Redirect para a confirmação de importação de dados
     *
     * @return void
     */
    public function importSuccess()
    {
        if(!empty($this->request->data)){
            
            $members=$this->Members->processCsvImport($this->request->data['uploadfile']);
            if(!empty($members)){

                $error_msg = [];
                foreach($members as $m){
                    $m['status'] = 1;
                    $entity = $this->Members->newEntity();
                    $entity = $this->Members->patchEntity($entity, $m, ['associated' => []]);
                    if($this->Members->save($entity)){
                        $fresh_member = $this->Members->get($entity->id);

                        //Start event trigger
                        $event = new Event('Model.Members.import_success', $this, [
                            'data'=>$fresh_member
                        ]);
                        $this->eventManager()->dispatch($event);

                    }else{
                        //Erros para o ecrã
                        if($entity->errors()){
                            
                            foreach( $entity->errors() as $errors){
                                if(is_array($errors)){
                                    foreach($errors as $error){
                                        $error_msg[]    =   $error;
                                    }
                                }else{
                                    $error_msg[]    =   $errors;
                                }
                            }
            
                        }
                    }
                }

                if(!empty($error_msg)){

                    $this->Flash->error(
                        __("Importação com os seguinte(s) erro(s): <p><strong>".implode("\n \r", $error_msg)."</strong></p>"),['escape' => false]
                    );
  
                    return $this->redirect(['action'=>'import']);
                }

            }

        }

        $this->Flash->success(__('Importação de '.count($members).' registo(s) ocorreu com sucesso.'));
        $this->render('/admin/Members/import');
    }


    /**
     * Exemplo de CSV a ser gerado
     */
    public function csvdemo()
    {
        $csv = $this->Members->csvexample();

    }


    /**
     * Aprova o membro
     * O estado é alterado para 1
     * e devolve o objecto do membro em caso de sucesso
     * @param string POST data member_id 
     * @return void
     */
    public function approve()
    {
        
        $result = '';
        if($this->request->is('post')){
            $member_id = $this->request->data['member_id'];
            if(!empty($member_id)){
                $result = $this->Members->approveMembershipProcess($member_id);
            }
        }else{
            throw new ForbiddenException(__("Apenas métodos POST."), 1);
        }

        if(!$this->request->is('json')){
            $this->redirect($this->referer());
        }


        $this->set(compact('result'));
        $this->set('_serialize', ['result']);
    }

}
