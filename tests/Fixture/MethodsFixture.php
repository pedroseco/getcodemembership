<?php
namespace GetcodeMembership\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GcMethodsFixture
 *
 */
class MethodsFixture extends TestFixture
{

    public $connection = 'test';
    public $import = ['table' => 'gc_methods'];

    // @codingStandardsIgnoreStart
    public $fields = [];
    // @codingStandardsIgnoreEnd
    
    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'title' => 'Paypal',
            'config_name' => 'Paypal',
            'description' => 'Metodo de pagamento Paypal',
            'status' => 1,
            'created' => '2017-05-12 18:22:21',
            'modified' => '2017-05-12 18:22:21'
        ],
        [
            'id' => 2,
            'title' => 'EasyPayMb',
            'config_name' => 'EasyPayMb',
            'description' => 'Metodo de pagamento EasyPayMb',
            'status' => 1,
            'created' => '2017-05-12 18:22:21',
            'modified' => '2017-05-12 18:22:21'
        ],
        [
            'id' => 3,
            'title' => 'Manual',
            'config_name' => 'Manual',
            'description' => 'Metodo de pagamento Manual',
            'status' => 1,
            'created' => '2017-05-12 18:22:21',
            'modified' => '2017-05-12 18:22:21'
        ],
        [
            'id' => 4,
            'title' => 'IfthenPay',
            'config_name' => 'Ifthenpay',
            'description' => 'Metodo de pagamento IfthenPay',
            'status' => 1,
            'created' => '2017-05-12 18:22:21',
            'modified' => '2017-05-12 18:22:21'
        ]
        
    ];
}
