<?php
namespace GetcodeMembership\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InvoiceitemsFixture
 *
 */
class InvoiceitemsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $connection = 'test';
    public $import = ['table' => 'gc_invoiceitems'];

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'invoice_id' => 'a7bd6be5-51e3-47dd-bd78-8e07a425dd8b',
            'description' => 'Descrição de item de Invoice',
            'item_id' => 1,
            'item_model' => 'Event',
            'item_title' => 'Lição de Vida',
            'item_total' => 20.99,
            'item_price' => 20.99,
            'quantity' => 1,
            'tax_id' => 1,
            'tax_name' => 'Iva 23',
            'tax_percentage' => 23,
            'created' => '2017-10-03 23:15:54',
            'modified' => '2017-10-03 23:15:54'
        ],
    ];
}
