<?php
namespace GetcodeMembership\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MembersFixture
 *
 */
class MembersFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $connection = 'test';
    public $import = ['table' => 'gc_members'];


    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'num' => 1,
            'name' => 'José Mourinho',
            'idnumber' => 1,
            'birthdate' => '2018-01-23',
            'local' => 'Manchester',
            'address' => 'Manchester',
            'phone' => 1,
            'email' => 'jose@mourinho.pt',
            'admission_date' => '2018-01-23',
            'status' => 1,
            'last_payment_date' => '2018-01-23',
            'last_payment_value' => 1.5,
            'created' => '2018-01-23 17:22:47',
            'modified' => '2018-01-23 17:22:47'
        ],[
            'id' => 2,
            'num' => 2,
            'name' => 'Lorem ipsum dolor sit amet',
            'idnumber' => '098098098',
            'birthdate' => '2018-01-23',
            'local' => 'Torres Vedras',
            'address' => 'Rua da Pizza',
            'phone' => 261431231,
            'email' => 'emaildemo@getcode.pt',
            'admission_date' => '2018-01-01',
            'status' => 0,
            'last_payment_date' => null,
            'last_payment_value' => null,
            'created' => '2018-01-01 17:22:47',
            'modified' => '2018-01-01 17:22:47'
        ],[
            'id' => 3,
            'num' => null,
            'name' => 'Lorem ipsum dolor sit amet',
            'idnumber' => '098098098',
            'birthdate' => '2018-01-23',
            'local' => 'Torres Vedras',
            'address' => 'Rua da Pizza',
            'phone' => 261431231,
            'email' => 'emaildemo@getcode.pt',
            'admission_date' => '2018-01-01',
            'status' => 1,
            'last_payment_date' => null,
            'last_payment_value' => null,
            'created' => '2018-01-01 17:22:47',
            'modified' => '2018-01-01 17:22:47'
        ]
    ];
}
