<?php
namespace GetcodeMembership\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InvoicesFixture
 *
 */
class InvoicesFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $connection = 'test';
    public $import = ['table' => 'gc_invoices'];

   
    // @codingStandardsIgnoreStart
    public $fields = [];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 'a7bd6be5-51e3-47dd-bd78-8e07a425dd8b',
            'invoice_number' => '000001',
            'invoicetpl_id' => 1,
            'billing_address' => 'Lorem ipsum dolor sit amet',
            'billing_city' => 'Lorem ipsum dolor sit amet',
            'billing_phone' => 'Lorem ipsum do',
            'billing_email' => 'Lorem ipsum dolor sit amet',
            'billing_postal' => 'Lorem ip',
            'customer_id' => 'Lorem ipsum dolor sit amet',
            'customer_name' => 'Lorem ipsum dolor sit amet',
            'customer_email' => 'Lorem ipsum dolor sit amet',
            'due_date' => '2017-10-03',
            'last_payment_date' => '2017-10-03',
            'balance' => 1.5,
            'total' => 1.5,
            'subtotal' => 1.5,
            'status' => 1,
            'link' => 'Lorem ipsum dolor sit amet',
            'created' => '2017-10-09 14:05:08'
        ],
        [
            'id' => 'a7bd6be5-51e3-43dd-bd78-8e07a425dd8b',
            'invoice_number' => null,
            'invoicetpl_id' => 1,
            'billing_address' => 'Lorem ipsum dolor sit amet',
            'billing_city' => 'Lorem ipsum dolor sit amet',
            'billing_phone' => 'Lorem ipsum do',
            'billing_email' => 'Lorem ipsum dolor sit amet',
            'billing_postal' => 'Lorem ip',
            'customer_id' => 'Lorem ipsum dolor sit amet',
            'customer_name' => 'Lorem ipsum dolor sit amet',
            'customer_email' => 'Lorem ipsum dolor sit amet',
            'due_date' => '2017-10-03',
            'last_payment_date' => '2017-10-03',
            'balance' => 29.99,
            'total' => 29.99,
            'subtotal' => 29.99,
            'status' => 1,
            'link' => 'Lorem ipsum dolor sit amet',
            'created' => '2017-10-09 15:05:08'
        ],
        [
            'id' => 'b7bd6be5-51e3-43dd-bd78-8e07a425dd8b',
            'invoice_number' => null,
            'invoicetpl_id' => 1,
            'billing_address' => 'Lorem ipsum dolor sit amet',
            'billing_city' => 'Lorem ipsum dolor sit amet',
            'billing_phone' => 'Lorem ipsum do',
            'billing_email' => 'Lorem ipsum dolor sit amet',
            'billing_postal' => 'Lorem ip',
            'customer_id' => 'Lorem ipsum dolor sit amet',
            'customer_name' => 'Lorem ipsum dolor sit amet',
            'customer_email' => 'Lorem ipsum dolor sit amet',
            'due_date' => '2017-10-03',
            'last_payment_date' => '2017-10-03',
            'balance' => 29.99,
            'total' => 29.99,
            'subtotal' => 29.99,
            'status' => 0,
            'link' => 'Lorem ipsum dolor sit amet',
            'created' => '2017-10-09 15:05:08'
        ]
    ];
}
