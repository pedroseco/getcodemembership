<?php
namespace GetcodeMembership\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PlansFixture
 *
 */
class PlansFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $connection = 'test';
    public $import = ['table' => 'gc_subscriptionplans'];


    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'code' => 'Plano mensal',
            'name' => 'Plano mensal',
            'description' => 'Descrição de plano',
            'status' => 1,
            'product_id' => 1,
            'tax_percentage' => 23,
            'tax_name' => 'IVA 23',
            'trial_period' => 1,
            'setup_fee' => 0,
            'recurring_price' => 5,
            'interval_num' => 1,
            'interval_unit' => 'month',
            'interval_unit_formatted' => 'Mês',
            'url' => '',
            'created' => '2018-01-23 18:29:26',
            'modified' => '2018-01-23 18:29:26'
        ],
        [
            'id' => 2,
            'code' => 'Plano semestral',
            'name' => 'Plano semestral',
            'description' => 'Descrição de plano',
            'status' => 1,
            'product_id' => 1,
            'tax_percentage' => 23,
            'tax_name' => 'IVA 23',
            'trial_period' => 1,
            'setup_fee' => 0,
            'recurring_price' => 5,
            'interval_num' => 6,
            'interval_unit' => 'month',
            'interval_unit_formatted' => 'Mês',
            'url' => '',
            'created' => '2018-01-23 18:29:26',
            'modified' => '2018-01-23 18:29:26'
        ],
    ];
}
