<?php
namespace GetcodeMembership\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GcTransactionsFixture
 *
 */
class TransactionsFixture extends TestFixture
{


    public $connection = 'test';
    public $import = ['table' => 'gc_transactions'];

    // @codingStandardsIgnoreStart
    public $fields = [];
    // @codingStandardsIgnoreEnd
    
    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'method_id' => 1,
            'amount' => 1.5,
            'method' => 'Lorem ipsum dolor sit amet',
            'status' => 1,
            'method_data' => null,
            'foreign_key' => 1,
            'foreign_model' => 'Lorem ipsum dolor sit amet',
            "method"=>[],
            'ip' => 'Lorem ipsum dolor sit amet',
            'customer_id'=>null,
            'customer_email'=> null,
            'customer_name'=> null,
            'invoice_id'=>null,
            'created' => '2017-05-12 18:20:45',
            'modified' => '2017-05-12 18:20:45'
        ],
        [
            'id' => 2,
            'method_id' => 1,
            'amount' => '29.99',
            'status' => 0,
            'method_data' => null,
            'foreign_key' => 2,
            'foreign_model' => 'Forms',
            "method"=>[],
            'ip' => null,
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'foreign_parent_model' => 'Events',
            'foreign_parent_id' => 5,
            'customer_id'=>null,
            'customer_email'=> null,
            'customer_name'=> null,
            'invoice_id'=>null
        ],
        [
            'id' => 3,
            'method_id' => 1,
            'amount' => '29.99',
            'status' => 0,
            'method_data' => null,
            'foreign_key' => 2,
            'foreign_model' => 'Forms',
            "method"=>[],
            'ip' => null,
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'foreign_parent_model' => 'Events',
            'foreign_parent_id' => 5,
            'customer_id'=>null,
            'customer_email'=> 'demo@demo.pt',
            'customer_name'=> 'Demo',
            'invoice_id'=>null
        ],
        [
            'id' => 4,
            'method_id' => 1,
            'amount' => '29.99',
            'status' => 0,
            'method_data' => null,
            'foreign_key' => 2,
            'foreign_model' => 'Forms',
            "method"=>[],
            'ip' => null,
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'foreign_parent_model' => 'Events',
            'foreign_parent_id' => 5,
            'customer_id'=>1,
            'customer_email'=> 'demo@demo.pt',
            'customer_name'=> 'Demo',
            'invoice_id'=>'a7bd6be5-51e3-43dd-bd78-8e07a425dd8b'
        ],
        [
            'id' => 5,
            'method_id' => 1,
            'amount' => '9.99',
            'status' => 0,
            'method_data' => null,
            'foreign_key' => 2,
            'foreign_model' => 'Forms',
            "method"=>[],
            'ip' => null,
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'foreign_parent_model' => 'Events',
            'foreign_parent_id' => 5,
            'customer_id'=>1,
            'customer_email'=> 'demo@demo.pt',
            'customer_name'=> 'Demo',
            'invoice_id'=>'a7bd6be5-51e3-43dd-bd78-8e07a425dd8b'
        ],
        [
            'id' => 6,
            'method_id' => 1,
            'amount' => '9.99',
            'status' => 0,
            'method_data' => 0,
            'foreign_key' => 2,
            'foreign_model' => 'Forms',
            "method"=>[],
            'ip' => null,
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'foreign_parent_model' => 'Events',
            'foreign_parent_id' => 5,
            'customer_id'=>1,
            'customer_email'=> 'demo@demo.pt',
            'customer_name'=> 'Demo',
            'invoice_id'=>'b7bd6be5-51e3-43dd-bd78-8e07a425dd8b'
        ],
        [
            'id' => 7,
            'method_id' => 2,
            'amount' => '9.99',
            'status' => 0,
            'method_data' => '{"amount":45,"description":"000018","transactionId":"a8d92647-b5e4-4fb0-a5ed-a3dda1b6f655","returnUrl":"http:\/\/demo.getcode.test\/payments\/success\/a8d92647-b5e4-4fb0-a5ed-a3dda1b6f655","cancelUrl":"http:\/\/demo.getcode.test\/payments\/cancel\/a8d92647-b5e4-4fb0-a5ed-a3dda1b6f655"}',
            'foreign_key' => 2,
            'foreign_model' => 'Forms',
            'ip' => null,
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'foreign_parent_model' => 'Events',
            'foreign_parent_id' => 5,
            'customer_id'=>1,
            'customer_email'=> 'demo@demo.pt',
            'customer_name'=> 'Demo',
            'invoice_id'=>'b7bd6be5-51e3-43dd-bd78-8e07a425dd81'
        ],
        [
            'id' => '61fe78c3-abf3-4812-a93d-92bcab6d94ec',
            'method_id' => 2,
            'amount' => '111',
            'status' => 0,
            'method_data' => null,
            'foreign_key' => 2,
            'foreign_model' => 'Forms',
            "method" => [],
            'ip' => null,
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'foreign_parent_model' => 'Events',
            'foreign_parent_id' => 5,
            'customer_id'=>1,
            'customer_email'=> 'demo@demo.pt',
            'customer_name'=> 'Demo',
            'invoice_id'=>'b7bd6be5-51e3-43dd-bd78-8e07a425dd81'
        ],
        [
            'id' => 'fd601b7f-f08c-4973-911a-ae96fe765698',
            'method_id' => 4,
            'amount' => '29.99',
            'status' => 0,
            'method_data' => '{"entidade":"12133","transactionReference":"992 000 329","valor":29.99}',
            'ip' => '192.168.33.1',
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'customer_id'=>null,
            'customer_email'=> null,
            'customer_name'=> null,
            'observations'=>'',
            'invoice_id'=>'b7bd6be5-51e3-43dd-bd78-8e07a425dd81'
        ],
        [
            'id' => 'fd601b7f-f08c-4973-911a-ae96fe765699',
            'method_id' => 4,
            'amount' => '9.99',
            'status' => 0,
            'method_data' => '{"entidade":"12133","transactionReference":"992 020 291","valor":9.99}',
            'ip' => '192.168.33.1',
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'customer_id'=>null,
            'customer_email'=> null,
            'customer_name'=> null,
            'observations'=>'',
            'invoice_id'=>'b7bd6be5-51e3-43dd-bd78-8e07a425dd83'
        ],
        [
            'id' => 'fd601b7f-f08c-4973-911a-ae96fe765603',
            'method_id' => 4,
            'amount' => '24.99',
            'status' => 0,
            'method_data' => '{"ep_status":"ok0","ep_message":"ep_country and ep_entity and ep_user and ep_cin ok and validation by code;code ok - new reference generated - 657600885 - ","ep_cin":"6576","ep_user":"ATV250717","ep_entity":"10611","ep_reference":"657600288","ep_value":"14.00","t_key":"087c556b-b51e-4d0b-a67d-d5111d7165c8","ep_link":"https:\/\/www.easypay.pt\/_s\/c11.php?e=10611&r=657600288&v=14.00&c=PT&l=PT&t_key=087c556b-b51e-4d0b-a67d-d5111d7165c8"}',
            'ip' => '192.168.33.1',
            'created' => '2017-09-26 09:44:35',
            'modified' => '2017-09-26 09:44:35',
            'customer_id'=>null,
            'customer_email'=> null,
            'customer_name'=> null,
            'observations'=>'',
            'invoice_id'=>'b7bd6be5-51e3-43dd-bd78-8e07a425dd83'
        ],
        
    ];
}