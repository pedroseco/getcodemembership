<?php
namespace GetcodeMembership\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SubscriptionsFixture
 *
 */
class SubscriptionsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $connection = 'test';
    public $import = ['table' => 'gc_subscriptions'];


    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [];

    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'status' => 1,
            'sub_total' => 10,
            'amount' => 10,
            'name' => 'Quota Mensal',
            'activated_at' => '2018-07-08 00:00:00',
            'current_term_starts_at' => '2018-07-08 00:00:00',
            'current_term_ends_at' => '2018-08-08 00:00:00',
            'last_billing_at' => '2018-07-08 00:00:00',
            'next_billing_at' => '2018-08-08 00:00:00',
            'plan_id' => 1,
            'member_id' => 1,
            'created' => '2018-01-23 17:34:28',
            'modified' => '2018-01-23 17:34:28'
        ],
        [
            'id' => 2,
            'name' => 'Quota Semestral',
            'status' => 1,
            'sub_total' => 60,
            'amount' => 60,
            'activated_at' => '2018-02-09 00:00:00',
            'current_term_starts_at' => '2018-02-09 00:00:00',
            'current_term_ends_at' => '2018-08-09 00:00:00',
            'last_billing_at' => '2018-02-09 00:00:00',
            'next_billing_at' => '2018-08-09 00:00:00',
            'plan_id' => 2,
            'member_id' => 1,
            'created' => '2018-01-23 17:34:28',
            'modified' => '2018-01-23 17:34:28'
        ],
        [
            'id' => 3,
            'name' => 'Quota Semestral #ciclo2',
            'status' => 1,
            'sub_total' => 60,
            'amount' => 60,
            'activated_at' => '2017-11-01 00:00:00',
            'current_term_starts_at' => '2018-04-01 00:00:00',
            'current_term_ends_at' => '2018-10-01 00:00:00',
            'last_billing_at' => '2018-04-01 00:00:00',
            'next_billing_at' => '2018-10-01 00:00:00',
            'plan_id' => 2,
            'member_id' => 1,
            'created' => '2018-01-23 17:34:28',
            'modified' => '2018-01-23 17:34:28'
        ],
    ];
}
