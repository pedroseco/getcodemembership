<?php
namespace GetcodeMembership\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use GetcodeMembership\Model\Table\SubscriptionsTable;

/**
 * GetcodeMembership\Model\Table\SubscriptionsTable Test Case
 */
class SubscriptionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \GetcodeMembership\Model\Table\SubscriptionsTable
     */
    public $Subscriptions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.GetcodeMembership.subscriptions',
        'plugin.GetcodeMembership.plans',
        'plugin.GetcodeMembership.members',
        'plugin.GetcodeMembership.invoices',
        'plugin.GetcodeMembership.invoiceitems'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Subscriptions') ? [] : ['className' => SubscriptionsTable::class];
        $this->Subscriptions = TableRegistry::get('Subscriptions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Subscriptions);
    
        parent::tearDown();
    }

    

    /**
     * Testa o calculo da data com Interval
     *
     * @return void
     */
    public function testCalculateEndOfTermDate()
    {
        $current_date = date('Y-m-d', strtotime('2018-02-27'));
        $sub = $this->Subscriptions->get(1, ['contain'=>['Plans']]);
      
        $result=$this->Subscriptions->calculateEndOfTermDate($current_date,$sub->plan->interval_num,$sub->plan->interval_unit);
        $this->assertEquals($result, '2018-03-27');
        
    }
  

    /**
     * So devolve o primeiro registo
     *
     * @return void
     */
    public function testFindSubscriptionsInEndOfTerm()
    {
        $target_date = '2018-08-08';
        $result = $this->Subscriptions->findSubscriptionsInEndOfTerm($target_date);

        $this->assertEquals($result->toArray()[0]['id'],1);

    }

    /**
     * atualiza uma subscrição a finalizar
     *
     * @return void
     */
    public function testUpdateSubscriptionsInEndOfTerm()
    {

        $target_date = '2018-05-01';
        $sub = $this->Subscriptions->find()->all();
     
        $result = $this->Subscriptions->updateSubscriptionsInEndOfTerm($sub,$target_date);

        $this->assertNotEmpty($result['id']);

    }

}
