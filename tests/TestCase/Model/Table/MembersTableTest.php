<?php
namespace GetcodeMembership\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use GetcodeMembership\Model\Table\MembersTable;

/**
 * GetcodeMembership\Model\Table\MembersTable Test Case
 */
class MembersTableTest extends TestCase
{
  public $Members;

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
    'plugin.GetcodeMembership.subscriptions',
    'plugin.GetcodeMembership.plans',
    'plugin.GetcodeMembership.members',
    'plugin.GetcodeMembership.invoices',
    'plugin.GetcodeMembership.invoiceitems'
  ];

      /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Members') ? [] : ['className' => MembersTable::class];
        $this->Members = TableRegistry::get('Members', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Members);
        parent::tearDown();
    }



    /**
     * testar o processo de aprovação
     *
     * @return void
     */
    public function testApproveMembership()
    {
        //Membro pendente
        $member_id=2;
        $m = $this->Members->approveMembershipProcess($member_id);
        $this->assertEquals($m->status,1);
    }

    /**
     * Testar o gerar novo numero de Membro
     *
     * @return void
     */
    public function testGenerateMemberNumber(){
        //Ultimo membro aprovado
        $number = $this->Members->generateMemberNumber();
        $this->assertEquals($number,2);
    }

    /**
     * Testar se o numero do membro já existe
     */
    public function testChangeMemberNumber()
    {
        $member = $this->Members->get(1);
        $number = 3;
        
        $change_member = $this->Members->changeMemberNumber($member,$number);
        $this->assertTrue($change_member);

    }


    /**
     * Testar se o numero do membro não existe
     */
    public function testChangeMemberNumberFalse()
    {
        $member = $this->Members->get(1);
        $number = 2;
        
        $change_member = $this->Members->changeMemberNumber($member,$number);
        $this->assertEquals($change_member,'Este número já se encontra atribuido a um Membro');

    }

}