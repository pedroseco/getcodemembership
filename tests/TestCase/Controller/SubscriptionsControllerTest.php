<?php
namespace GetcodeMembership\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use GetcodePayments\Controller\TransactionsController;
use Cake\Event\Event;
use Cake\Event\EventManager;
use Cake\Event\EventList;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\I18n\Time;

/**
 * GetcodeForms\Controller\FormsController Test Case
 */
class SubscriptionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.GetcodeMembership.subscriptions',
        'plugin.GetcodeMembership.plans',
        'plugin.GetcodeMembership.members',
        'plugin.GetcodeMembership.invoices',
        'plugin.GetcodeMembership.invoiceitems',
        'plugin.GetcodeMembership.methods',
        'plugin.GetcodeMembership.transactions'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->Subscriptions = TableRegistry::get('GetcodeMembership.Subscriptions');  
        $this->Invoices = TableRegistry::get('GetcodePayments.Invoices');        
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Subscriptions);
        parent::tearDown();
    }



    public function testSubscriptionRenewCurrentTermStarts()
    {
      
        $current_date = date('Y-m-d', strtotime('2018-08-08'));
        
        //Procura por subscrições que chegam ao fim á data
        $finishing_subscriptions = $this->Subscriptions->findSubscriptionsInEndOfTerm($current_date);
        //Edita e corre as ações necessárias a estas subscrições
        $update_subscriptions = $this->Subscriptions->updateSubscriptionsInEndOfTerm($finishing_subscriptions,$current_date,false);

        $s = $this->Subscriptions->get($update_subscriptions);        
        $this->assertEquals($s->current_term_starts_at, new Time('2018-08-08 00:00:00'));
      
    }



    public function testSubscriptionRenewCycle()
    {
      
        $current_date = date('Y-m-d', strtotime('2018-10-01'));

        $finishing_subscriptions=$this->Subscriptions->find()
            //->contain(['Members','Invoices'])
            ->where([
                'Subscriptions.status'=>1,
                'Subscriptions.id' => 3
                ])
            ->select([
                'Subscriptions.id',
                'Subscriptions.status',
                'Subscriptions.amount',
                'Subscriptions.plan_id',
                'Subscriptions.member_id',
                'Subscriptions.current_term_starts_at',
                'Subscriptions.current_term_ends_at',
                'Subscriptions.next_billing_at',
                'Subscriptions.expires_at'
            ])
            ->all();

        
        //Edita e corre as ações necessárias a estas subscrições
        $update_subscriptions = $this->Subscriptions->updateSubscriptionsInEndOfTerm($finishing_subscriptions,$current_date,false);

        $s = $this->Subscriptions->get($update_subscriptions);   
        
        $this->assertEquals($s->current_term_starts_at, new Time('2018-10-01 00:00:00'));
        $this->assertEquals($s->current_term_ends_at, new Time('2019-04-01 00:00:00'));
      
    }


    public function testSubscriptionRenewInvoice()
    {
      
        $current_date = date('Y-m-d', strtotime('2018-10-01'));

        $finishing_subscriptions=$this->Subscriptions->find()
            //->contain(['Members','Invoices'])
            ->where([
                'Subscriptions.status'=>1,
                'Subscriptions.id' => 3
                ])
            ->select([
                'Subscriptions.id',
                'Subscriptions.status',
                'Subscriptions.amount',
                'Subscriptions.plan_id',
                'Subscriptions.member_id',
                'Subscriptions.current_term_starts_at',
                'Subscriptions.current_term_ends_at',
                'Subscriptions.next_billing_at',
                'Subscriptions.expires_at'
            ])
            ->all();

        
        //Edita e corre as ações necessárias a estas subscrições
        $update_subscriptions = $this->Subscriptions->updateSubscriptionsInEndOfTerm($finishing_subscriptions,$current_date);
        $s = $this->Subscriptions->get($update_subscriptions);

        $invoice = $this->Invoices->find()->where(['foreign_key'=>$s->id])->contain(['Invoiceitems'])->first();

        $this->assertEquals($invoice->invoiceitems[0]->description, 'Plano semestral | (2018-01-04 - 2018-01-10)');
 
   }


}