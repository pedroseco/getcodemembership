<?php
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Core\Plugin;
use Cake\Event\EventManager;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

use GetcodeMembership\Event\MembersListener;
use GetcodeMembership\Event\SubscriptionsListener;


//Registar os Eventos no global event
$MembersListener = new MembersListener();
EventManager::instance()->on($MembersListener);
$SubscriptionsListener = new SubscriptionsListener();
EventManager::instance()->on($SubscriptionsListener);

$subscriptions_status=[
    0=> __('Inativo'),
    1=> __('Ativo'),
    2=> __('Cancelado'),
    3=>__('Removido')
];
Configure::write('subscriptions_status',$subscriptions_status);

$member_status=[
    0=> __('Inativo'),
    1=> __('Ativo'),
    2=> __('Cancelado')
];
Configure::write('member_status',$member_status);

$plan_status=[
    0=> __('Inativo'),
    1=> __('Ativo'),
    2=> __('Cancelado')
];
Configure::write('plan_status',$plan_status);

$gender=[
    'f'=> __('Feminino'),
    'm'=> __('Masculino')
];
Configure::write('gender',$gender);

$interval_unit=[
    'day'=> __('Dia(s)'),
    'week'=> __('Semana(s)'),
    'month'=> __('Mes(es)'),
    'year'=> __('Ano(s)')
];

Configure::write('interval_unit',$interval_unit);

$csv_table_headers=[
    'num'=>'NºSÓCIO',
    'name'=>'NOME',
    'birthdate'=>'DATA NASCIMENTO',
    'local'=>'LOCALIDADE',
    'address'=>'MORADA',
    'idnumber'=>'NºIDENTIFICAÇÃO',
    'phone'=>'TELEFONE',
    'email'=>'EMAIL',
    'admission_date'=>'DATA ADMISSÃO',
    'status'=>'ESTADO',
    'last_payment_date'=>'DATA ÚLTIMO PAGAMENTO',
    'last_payment_value'=>'VALOR ÚLTIMO PAGAMENTO',
    'user_id'=>'user_id'
];
Configure::write('csv_table_headers',$csv_table_headers);

/**
 * Definições de opções
 */
//N dias que validamos as renovações
Configure::write('Settings.Subscriptions.renewal_days_before',5);