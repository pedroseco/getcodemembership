<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'GetcodeMembership',
    ['path' => '/gc-membership'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

Router::prefix('admin', function ($routes) {
    $routes->extensions(['json','csv']);

    $routes->plugin('GetcodeMembership', ['path' => '/gc-membership'], function ($routes) {
        $routes->fallbacks(DashedRoute::class);
    });
    
    $routes->plugin('GetcodeMembership', ['path' => '/'], function ($routes) {
        $routes->fallbacks(DashedRoute::class);

        $routes->connect('/membros', ['controller' => 'members', 'action' => 'index']);
        $routes->connect('/membros/:action/*', ['controller'=>'members']);
        $routes->connect('/membros/add', ['controller'=>'members','action'=>'add']);
        $routes->connect('/membros/edit/:id', ['controller'=>'members','action'=>'edit'], ['pass'=>['id']]);

        $routes->connect('/subscricoes', ['controller' => 'subscriptions', 'action' => 'index']);
        $routes->connect('/subscricoes/:action/*', ['controller'=>'subscriptions']);
        $routes->connect('/subscricoes/add', ['controller'=>'subscriptions','action'=>'add']);
        $routes->connect('/subscricoes/edit/:id', ['controller'=>'subscriptions','action'=>'edit'], ['pass'=>['id']]);

        $routes->connect('/planos', ['controller' => 'plans', 'action' => 'index']);
        $routes->connect('/planos/:action/*', ['controller'=>'plans']);
        $routes->connect('/planos/add', ['controller'=>'plans','action'=>'add']);
        $routes->connect('/planos/edit/:id', ['controller'=>'plans','action'=>'edit'], ['pass'=>['id']]);


    });
});