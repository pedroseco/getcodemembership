<?php
use Migrations\AbstractMigration;

class AddTaxPercentageToSubscriptions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gc_subscriptions');
        $table->addColumn('tax_percentage', 'integer', [
            'default' => null,
            'limit' => 3,
            'null' => false,
        ]);
        $table->update();
    }
}
