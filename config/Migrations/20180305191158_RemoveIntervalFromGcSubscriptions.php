<?php
use Migrations\AbstractMigration;

class RemoveIntervalFromGcSubscriptions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gc_subscriptions');
        $table->removeColumn('interval_num');
        $table->removeColumn('interval_unit_formatted');
        $table->removeColumn('interval_unit');
        $table->update();
    }
}
