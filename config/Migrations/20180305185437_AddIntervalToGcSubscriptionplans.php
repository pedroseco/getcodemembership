<?php
use Migrations\AbstractMigration;

class AddIntervalToGcSubscriptionplans extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gc_subscriptionplans');
        $table->addColumn('interval_num', 'integer', [
            'default' => null,
            'limit' => 3,
            'null' => false,
        ]);
        $table->addColumn('interval_unit', 'string', [
            'default' => null,
            'limit' => 20,
            'null' => false,
        ]);
        $table->addColumn('interval_unit_formatted', 'string', [
            'default' => null,
            'limit' => 15,
            'null' => true,
        ]);
        $table->update();
    }
}
