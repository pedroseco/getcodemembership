<?php
use Migrations\AbstractMigration;

class CreateGcSubscription extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gc_subscriptions');
        $table->addColumn('status', 'integer', [
            'default' => null,
            'limit' => 1,
            'null' => false,
        ]);
        $table->addColumn('sub_total', 'decimal', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('amount', 'decimal', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('current_term_starts_at', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('current_term_ends_at', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('last_billing_at', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('next_billing_at', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('plan_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('member_id', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
