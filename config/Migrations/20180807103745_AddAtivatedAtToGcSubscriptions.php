<?php
use Migrations\AbstractMigration;

class AddAtivatedAtToGcSubscriptions extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gc_subscriptions');
        $table->addColumn('activated_at', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
