<?php
use Migrations\AbstractMigration;

class AddExtraFieldsToMembers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('gc_members');
        $table->addColumn('extra_fields', 'text', [
            'default' => null,
            'null' => true,
        ]);
        $table->update();
    }
}
